<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    protected $table = "achievements";
    protected $fillable = [
        'name',
        'description',
        'task_id',
        'user_id',
    ];


    public function task()
    {
        return $this->belongsTo(Task::class,'task_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
