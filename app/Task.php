<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = "tasks";
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'description',
        'user_id',
        'department_id', 'progress', 'IsDone',
    ];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function achievement()
    {
        return $this->hasMany(Achievement::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
