<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCommunication extends Model
{
    protected $table = "user_comunication";
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'type',
        'value',
        'user_id',
        'created_at',
        'updated_at',
    ];

}
