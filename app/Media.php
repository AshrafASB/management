<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Media extends Model
{
    protected $table = "media";
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'description',
        'url',
        'size',
        'type',
        'user_id',
        'created_at',
        'updated_at',
    ];

    /*    public function user()
        {
            return $this->belongsTo(User::class);
        }*/
}
