<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Role extends Model
{
    protected $table = "roles";
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'id',
        'name',
        'description',
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }

}
