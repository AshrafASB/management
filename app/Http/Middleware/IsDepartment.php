<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsDepartment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return abort(403);
        }

        if(Auth::user()->role->name !='Department'){
            return back()->with('warning', 'عذراً: لا يوجد لديك صلاحية');
        }
        return $next($request);
    }
}
