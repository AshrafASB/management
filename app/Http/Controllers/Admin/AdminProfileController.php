<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\profileRequest;
use App\Media;
use App\Profile;
use App\User;
use App\UserInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminProfileController extends Controller
{

    public function index()
    {
        $data['item'] = User::with(['information', 'communication'])->find(Auth::user()->id);
        return view('admin.profile.index');
    }

    public function create()
    {
        //
    }

    public function imageUploadPost(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        //$uploadedFile = $request->file('file');
        $uploadedFile = $request->file('file')->getSize();
//        dd($uploadedFile);

        $fileSize = $uploadedFile->getClientsize();
        $filename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        $filename = str_replace(' ', '_', $filename);
        $extension = $uploadedFile->getClientOriginalExtension();


//        $extension=$request->image->extension();
//        $name=$request->image->();
        $imageName = time() . '.' . $request->image->extension();

        $request->image->move(public_path('images'), $imageName);


        try {
            dd($request->toArray());
            Media::create([
                'name' => $filename,
                'description' => $filename,
                'type' =>  $extension,
                'url' => $imageName,
                'user_id' => Auth::user()->id,
                'size' =>$fileSize,
            ]);
            /*       return redirect()->route('admin.Profile.index')
                       ->with('success', 'تمت عملية الإضافة بنجاح');*/

            return back()
                ->with('success', 'You have successfully upload image.')
                ->with('image', $imageName);
        } catch (\Exception $exception) {
            return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
        }



    }

    public function store(profileRequest $request)
    {

        $user = Profile::where('user_id', Auth::user()->id)->first();
        if ($user) {
            // dd(UserInformation::whereId($user->id)->update($request->all())->toSql());

            try {
                UserInformation::whereId($user->id)->update(['id_number' => $request['id_number'],
                    'f_name' => $request['f_name'],
                    'l_name' => $request['l_name'],
                    'DOB' => $request['DOB'],
                    'POB' => $request['POB'],
                    'user_id' => Auth::user()->id]);
                return redirect()->route('admin.Profile.index')
                    ->with('success', 'تمت عملية الإضافة بنجاح');
            } catch (\Exception $exception) {
                return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
            }
        } else {
            try {
                //UserInformation::create($request->all());
                UserInformation::create([
                    'id_number' => $request['id_number'],
                    'f_name' => $request['f_name'],
                    'l_name' => $request['l_name'],
                    'DOB' => $request['DOB'],
                    'POB' => $request['POB'],
                    'user_id' => Auth::user()->id,
                ]);
                return redirect()->route('admin.Profile.index')
                    ->with('success', 'تمت عملية الإضافة بنجاح');
            } catch (\Exception $exception) {
                return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
            }
        }


    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
