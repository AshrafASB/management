<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\Http\Controllers\Controller;
use App\Set;
use App\Task;
use Illuminate\Http\Request;

class AdminTaskController extends Controller
{

    public function index()
    {
        /*        $all_department = Department::where('set_id', self::getSetID())->pluck('id');

                $data['data'] = Task::with(['department', 'achievement'])
                    ->whereIn('department_id', $all_department)->get();*/
        $data['data'] = Task::with(['department.set', 'achievement'])->get();
        return view('admin.task.index', $data);

        /*        $items = Task::orderByDesc('created_at')->paginate(15)->appends(request()->query());
                return view('admin.task.index', compact('items'));*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $date['department'] = Department::all()->pluck('name', 'id');
        $date['set'] = Set::all()->pluck('name', 'id');
        return view('admin.task.create', $date);
    }

    public function store(Request $request)
    {
//        dd($request->all());
        try {
            Task::create($request->all());
            return redirect()->route('admin.task.index')
                ->with('success', 'تمت عملية الإضافة بنجاح');
        } catch (\Exception $exception) {
            return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
        }
    }

//    public function show($id)
//    {
//        //
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        try {
            $delteTask =  Task::findOrFail($id);
            $delteTask->delete();
            return redirect()->route('admin.task.index')->with('success',"Deleted [ $delteTask->name ] Successful");

        }catch (\Exception $exception){
            return redirect()->route('admin.task.index')->with('error','حدث خطأ أثناء عملية الحذف:'.$exception);
        }
    }
}
