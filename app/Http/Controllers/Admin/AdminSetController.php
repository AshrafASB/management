<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SetRequest;
use App\Set;
use Illuminate\Http\Request;

class AdminSetController extends Controller
{

    /*    public function __construct()
        {
            parent::__construct();
            self::$data['active_menu'] = 'user';
        }*/

    public function index()
    {
        $data['data'] = Set::all();
        return view('admin.set.index', $data);
    }


    public function create()
    {
        return view('admin.set.create');
    }


    public function store(SetRequest $request)
    {
        try {
            Set::create($request->all());
            return redirect()->route('admin.set.index')
                ->with('success', 'تمت عملية الإضافة بنجاح');
        } catch (\Exception $exception) {
            return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
        }
    }


//    public function show($id)
//    {
//        //
//    }


    public function edit(Set $set)
    {
        return view('admin.set.edit',compact('set'));
    }


    public function update(Request $request, $id)
    {
        try {
            $updateSet = Set::findOrFail($id);
            $updateSet->name = $request->input('name');
            $updateSet->description = $request->input('description');

            $updateSet->update();
            return redirect()->route('admin.set.index')
                ->with('success',"Set $updateSet->name OR $updateSet->description updated Successfully");
        }catch (\Exception $exception){
            return redirect()->route('admin.set.edit',[$id])->with('error','Update Fail');
        }
    }


    public function destroy($id)
    {
        try {
            $delteSet =  Set::findOrFail($id);
            $delteSet->delete();
            return redirect()->route('admin.set.index')->with('success',"Deleted [ $delteSet->name ] Successful");

        }catch (\Exception $exception){
            return redirect()->route('admin.set.index')->with('error','حدث خطأ أثناء عملية الحذف:'.$exception);
        }
    }
}
