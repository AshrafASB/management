<?php

namespace App\Http\Controllers\Admin;

use App\Achievement;
use App\Http\Controllers\Controller;
use App\Massage;
use App\Message;
use App\User;
use Illuminate\Http\Request;

class AdminMessageController extends Controller
{

    public function __construct()
    {
        $this->path = 'admin.messages.';
    }

    public function index()
    {
        return view($this->path . 'index', [
            'messages' => Message::orderBy('id', 'desc')->get()
        ]);
    }//end of index

    public function create()
    {

        return view($this->path . 'create',
        [
            'users' => User::where('id','!=',self::authId())->get()
        ]);
    }//end of create


    public function store(Request $request)
    {
//dd($request->alذl());
        try {
            Message::create([
                "from" => self::authId(),
                'to' => $request->to,
                'title' => $request->title,
                'description' => $request->description,
                'isRead' => false,
                'spam' => false,
                'isStarred' =>  false,
         ]   );
            return redirect()->route('admin.messages.index')
                ->with('success', 'تمت عملية الإضافة بنجاح');
        } catch (\Exception $exception) {
            dd($exception);
            return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
        }

    }//end of store


    public function show(Message $message)
    {
       return view('admin.messages.show',[
           'msg' => $message
       ]);
    }//end of show


    public function edit($id)
    {
        //
    }//end of edit


    public function update(Request $request, $id)
    {
        //
    }//end of update


    public function destroy($id)
    {
        //
    }//end of destroy
}
