<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\userRequest;
use App\Role;
use App\Set;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminUserController extends Controller
{

    /*    public function __construct()
        {
            parent::__construct();
            self::$data['active_menu'] = 'user';
        }*/

    public function index()
    {
        $data['data'] = User::with(['department','set','role'])->get();

        return view('admin.user.index', $data);
    }


    public function create()
    {
        $data['role'] = Role::all()->pluck('name', 'id');
        $data['set'] = Set::all();
        $data['department'] = Department::all();
        return view('admin.user.create', $data);
    }


    public function store(userRequest $request)
    {

        try {
            User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'department_id' => $request->input('department_id'),
                'set_id' => $request->input('set_id'),
                'role_id' => $request->input('role_id')

            ]);
            return redirect()->route('admin.user.index')->with('success', 'تمت عملية الإضافة بنجاح');
        } catch (\Exception $exception) {
            return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try {
            $updateUser = User::with(['department','set','role'])->findOrFail($id);
//            dd($updateUser->id);
            $data['role'] = Role::all()->pluck('name', 'id');
            $data['set'] = Set::all();
            $data['department'] = Department::all();
            return view('admin.user.edit',$data)->with('updateUser',$updateUser);
        }catch (\Exception $exception){
//            dd($exception);
            abort(404);
        }

    }


    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|min:3|max:50',
            'email' => 'email',
            'password'=>'required|confirmed',
            'password_confirmation'=>'sometimes|required_with:password',
        ]);

        try {
            $updateUser = User::findOrFail($id);
            $updateUser->name = $request->input('name');
            $updateUser->email = $request->input('email');
            $updateUser->password =  Hash::make($request['password']);
            $updateUser->set_id = $request->input('set_id');
            $updateUser->role_id = $request->input('role_id');
            $updateUser->department_id = $request->input('department_id');
            $updateUser->update();
            return redirect()->route('admin.user.index')
                ->with('success',"تم تحدسث بيانات المستخدم بنجاح");
        }catch (\Exception $exception){
            dd($exception);
            return redirect()->route('admin.user.edit',[$id])->with('error','حدث خطأ أثناء عملية النعديل '.$exception);

        }
    }


    public function destroy($id)
    {
        try {
            $delteUser =  User::findOrFail($id);
            $delteUser->delete();
            return redirect()->route('admin.user.index')->with('success',"Deleted [ $delteUser->name ] Successful");

        }catch (\Exception $exception){
            return redirect()->route('admin.user.index')->with('error','حدث خطأ أثناء عملية الحذف:'.$exception);
        }
    }
}
