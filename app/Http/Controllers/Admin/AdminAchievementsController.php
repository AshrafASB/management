<?php

namespace App\Http\Controllers\Admin;

use App\Achievement;
use App\Http\Controllers\Controller;
use App\Task;
use Illuminate\Http\Request;

class AdminAchievementsController extends Controller
{

    public function index()
    {
        $data['data'] = Achievement::with(['task.department.set', 'user'])->get();

        return view('admin.achievement.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $date['tasks'] = Task::all()->pluck('name', 'id');
        return view('admin.achievement.create', $date);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request['user_id']=self::authId();
       // $request->request->add(['user_id' => self::authId()]);
       // dd($request->toArray());
     /*   ['user_id' => self::authId(),
            'task_id' => $request->task_id,
            'name' => $request->task_id,
            'description' => $request->description]*/
        try {
            Achievement::create($request->all());
            return redirect()->route('admin.achievement.index')
                ->with('success', 'تمت عملية الإضافة بنجاح');
        } catch (\Exception $exception) {
            return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
        }
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        try {
            $delteAch = Achievement::findOrFail($id);
            $delteAch->delete();
            return redirect()->route('admin.achievement.index')->with('success',"Deleted [ $delteAch->name ] Successful");

        }catch (\Exception $exception){
            return redirect()->route('admin.achievement.index')->with('error','حدث خطأ أثناء عملية الحذف:'.$exception);
        }
    }
}
