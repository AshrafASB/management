<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CommunicationRequest;
use App\Media;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['item'] = User::with(['information', 'communication', 'medias'])->find(Auth::user()->id);

        return view('admin.communication.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CommunicationRequest $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $uploadedFile = $request->file('file');

        $fileSize = $uploadedFile->getSize();
        $filename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        $filename = str_replace(' ', '_', $filename);
        $extension = $uploadedFile->getClientOriginalExtension();

//        $extension=$request->image->extension();
//        $name=$request->image->();
        $imageName = time() . '.' . $request->image->extension();

        $request->image->move(public_path('images'), $imageName);


        try {
            dd($request->toArray());
            Media::create([
                'name' => $filename,
                'description' => $filename,
                'type' =>  $extension,
                'url' => $imageName,
                'user_id' => Auth::user()->id,
                'size' =>$fileSize,
            ]);
            /*       return redirect()->route('admin.Profile.index')
                       ->with('success', 'تمت عملية الإضافة بنجاح');*/

            return back()
                ->with('success', 'You have successfully upload image.')
                ->with('image', $imageName);
        } catch (\Exception $exception) {
            return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
