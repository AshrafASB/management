<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\departmentRequest;
use App\Set;
use App\User;
use Illuminate\Http\Request;

class AdminDepartmentController extends Controller
{

    public function index()
    {
        $data['data'] = Department::all();
        return view('admin.department.index', $data);
    }


    public function create()
    {
        $data['sets'] = Set::all()->pluck('name','id');
        return view('admin.department.create', $data);
    }


    public function store(departmentRequest $request)
    {
        try {
            Department::create($request->all());
            return redirect()->route('admin.department.index')
                ->with('success', 'تمت عملية الإضافة بنجاح');
        } catch (\Exception $exception) {
            return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
        }
    }


    public function show($id)
    {
        return view('admin.department.show');

    }


    public function edit(Department $department,Set $set)
    {
        $sets = Set::get();
        $department = Department::all()->first();

        return view('admin.department.edit',compact('sets','department'));
    }


    public function update(Request $request, $id)
    {
        try {
            $updateDep = Department::findOrFail($id);
            $updateDep->name = $request->input('name');
            $updateDep->description = $request->input('description');

            $updateDep->update();
            return redirect()->route('admin.department.index')
                ->with('success',"Department $updateDep->name OR $updateDep->description updated Successfully");
        }catch (\Exception $exception){
            return redirect()->route('admin.department.edit',[$id])->with('error','Update Fail');
        }
    }


    public function destroy($id)
    {
        try {
            $delteDep =  Department::findOrFail($id);
            $delteDep->delete();
            return redirect()->route('admin.department.index')->with('success',"Deleted [ $delteDep->name ] Successful");

        }catch (\Exception $exception){
            return redirect()->route('admin.user.index')->with('error','حدث خطأ أثناء عملية الحذف:'.$exception);
        }
    }
}
