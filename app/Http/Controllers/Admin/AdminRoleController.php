<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RoleRequest;
use App\Role;
use Illuminate\Http\Request;

class AdminRoleController extends Controller
{

    /*    public function __construct()
        {
            parent::__construct();
            self::$data['active_menu'] = 'user';
        }*/

    public function index()
    {
        $data['data'] = Role::all();
        return view('admin.role.index', $data);
    }


    public function create()
    {
        return view('admin.role.create');
    }


    public function store(RoleRequest $request)
    {

        try {
            Role::create($request->all());
            return redirect()->route('admin.role.index')
                ->with('success', 'تمت عملية الإضافة بنجاح');
        } catch (\Exception $exception) {
            return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
        }

    }


    public function edit(Role $role)
    {
        return view('admin.role.edit',compact('role'));
    }


    public function update(Request $request, $id)
    {
        try {
            $updateRole = Role::findOrFail($id);
            $updateRole->name = $request->input('name');
            $updateRole->description = $request->input('description');

            $updateRole->update();
            return redirect()->route('admin.role.index')
                ->with('success',"Set $updateRole->name OR $updateRole->description updated Successfully");
        }catch (\Exception $exception){
            return redirect()->back()->with('error','Update Fail');
        }
    }


    public function destroy($id)
    {
        try {
            $delteRole =  Role::findOrFail($id);
            $delteRole->delete();
            return redirect()->route('admin.role.index')->with('success',"Deleted [ $delteRole->name ] Successful");

        }catch (\Exception $exception){
            return redirect()->route('admin.role.index')->with('error','حدث خطأ أثناء عملية الحذف:'.$exception);
        }
    }
}
