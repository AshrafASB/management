<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Task;
use Illuminate\Http\Request;

class UserTasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['data'] = Task::with(['user'])->where('department_id', self::getAuthDepartmentID())->get();
        return view('user.task.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['tasks'] = Task::where('department_id', self::getAuthDepartmentID())->pluck('name', 'id');
        return view('user.task.create', $data);
    }

}
