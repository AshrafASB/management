<?php

namespace App\Http\Controllers\User;

use App\Achievement;
use App\Http\Controllers\Controller;
use App\Task;
use Illuminate\Http\Request;

class UserAchievementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['data'] = Achievement::with(['task', 'user'])->where('user_id', self::authId())->get();
        return view('user.achievement.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['tasks'] = Task::where('department_id', self::getAuthDepartmentID())->pluck('name', 'id');
        return view('user.achievement.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            Achievement::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'user_id' => self::authId(),
                'task_id' => $request->input('task_id'),
            ]);
            return redirect()->route('user.achievement.index')
                ->with('success', 'تمت عملية الإضافة بنجاح');
        } catch (\Exception $exception) {
            dd($request);
            return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $depId = Achievement::findOrFail($id);
            return view('user.achievement.edit')->with('depId', $depId);
        } catch (\Exception $exception) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {

        try {
            $updateDep = Achievement::findOrFail($id);
            $updateDep->name = $request->input('name');
            $updateDep->description = $request->input('description');

            $updateDep->update();
            return redirect()->route('user.achievement.index')
                ->with('success', "Department $updateDep->name OR $updateDep->description updated Successfully");
        } catch (\Exception $exception) {
            return redirect()->route('user.achievement.edit', [$id])->with('error', 'Update Fail');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
//        dd(1);
        try {
            $deleteID = Achievement::findOrFail($id);
            $deleteID->delete();
            return redirect()->route('user.achievement.index')->with('success', "تم حذف القسم بنجاح");
        } catch (\Exception $exception) {
//            dd(1);
            return redirect()->route('$deleteID')->with('error', 'عذرا !! هناك خطأ في عملية الحذف' . $exception);
        }

    }

}
