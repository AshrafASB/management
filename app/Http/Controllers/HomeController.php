<?php

namespace App\Http\Controllers;

use App\Set;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if (Auth::user()->role->name == 'admin') {
            $data['items'] = Set::with(['department.task', 'user'])->get();
        } elseif (Auth::user()->role->name == 'set') {
            $data['items'] = Set::with(['department.task', 'user'])->where('id', self::getSetID())->get();
        } elseif (Auth::user()->role->name == 'department') {
            $data['items'] = Set::with(['department.task', 'user'])->where('id', self::getSetID())->get();
        } elseif (Auth::user()->role->name == 'user') {
            $data['items'] = Set::with(['department.task', 'user'])->where('id', self::getSetID())->get();
            //$data['items'] = '';
        }

        $url = Auth::user()->role->name;

        return view($url . '.home', $data);

    }
}
