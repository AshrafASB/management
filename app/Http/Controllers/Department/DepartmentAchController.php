<?php

namespace App\Http\Controllers\Department;

use App\Achievement;
use App\Http\Controllers\Controller;
use App\Task;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DepartmentAchController extends Controller
{
    public function __construct()
    {

        $this->title_en = 'ach';
        $this->title_ar = 'الانجاز';
        $this->path = 'Department.Ach.';
    }

    public function index()
    {
        return view($this->path . 'index', [
            'Achs' => Achievement::all()
        ]);
    }//end of index

    public function create()
    {
        return view($this->path . 'create', [
            'tasks' => Task::where('department_id', '=', self::getAuthDepartmentID())->get()
        ]);
    }//end of create


    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), $this->rules(), $this->messages());
        if ($request->task < 0) {
            $text = "e:اختر مهمة ";
            return response(['status' => false, 'close' => 0, 'msg' => $text, 'title' => $this->title_ar]);

        }
        if ($validate->fails()) {
            $text = "e:تاكد من البيانات";
//            dd($text);

            return response(['status' => false, 'errors' => $validate->messages(), 'msg' => $text], 422);
        } else {
            try {
                $ads = Achievement::create([
                    'name' => $request->name,
                    "description" => $request->description,
                    "task_id" => $request->task,
                    "user_id" => self::authId(),
                ]);

                $text = "s:تمت اضافة " . $this->title_ar . " بنجاح";
                return response(['status' => true, 'close' => 1, 'msg' => $text, 'title' => $this->title_ar]);
            } catch (QueryException $queryException) {
                $text = "e:لم يتم اضافة " . $this->title_ar . " بنجاح";
                return response(['status' => false, 'close' => 0, 'msg' => $text, 'title' => $this->title_ar]);
            }
        }
    }




    public function edit(Achievement $ach)
    {
        return view($this->path . 'edit', [
            'ach'=> $ach,
            'tasks' => Task::where('department_id', '=', self::getAuthDepartmentID())->get(),
        ]);
    }



    public function update(Request $request, Achievement $ach)
    {
        $validate = Validator::make($request->all(), $this->rules($ach->id), $this->messages($ach->id));

        if ($request->task < 0) {
            $text = "e:اختر مهمة ";
            return response(['status' => false, 'close' => 0, 'msg' => $text, 'title' => $this->title_ar]);

        }


        if ($validate->fails()) {
            $text = "e:تاكد من البيانات";
//            dd($text);

            return response(['status' => false, 'errors' => $validate->messages(), 'msg' => $text], 422);
        } else {
            try {

                $ach->update([
                    'name' => $request->name,
                    "description" => $request->description,
                    "task_id" => $request->task,
                ]);

                $text = "s:تمت اضافة " . $this->title_ar . " بنجاح";
                return response(['status' => true, 'close' => 1, 'msg' => $text, 'title' => $this->title_ar]);
            } catch (QueryException $queryException) {
                $text = "e:لم يتم اضافة " . $this->title_ar . " بنجاح";
                return response(['status' => false, 'close' => 0, 'msg' => $text, 'title' => $this->title_ar]);
            }
        }
    }


    public function destroy(Achievement $ach)
    {
        try {

            $ach->delete();
            $text = "s: تمت الحذف  " . $this->title_ar . " بنجاح";
            return response(['status' => true, 'close' => 1, 'msg' => $text, 'title' => $this->title_ar]);
        } catch (QueryException $queryException) {
            $text = "e:المهمة غير موجود";
            return response(['status' => false, 'close' => 0, 'msg' => $text, 'title' => $this->title_ar]);
        }
    }



    private function rules($id = null)
    {
        $rules = [
            'name' => 'required|min:3',
            'description' => 'required',
        ];

        return $rules;

    }

    private function messages($id = null)
    {
        return [
            'name.required' => 'الاسم مطلوب',
            'name.min' => 'الاسم يجب ان يحتوي ع الاقل 3 احرف',

        ];
    }


}
