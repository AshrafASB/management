<?php

namespace App\Http\Controllers\Department;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class DepartmentUserController extends Controller
{

    public function __construct()
    {

        $this->title_en = 'Users';
        $this->title_ar = 'المستخدمين';
        $this->path = 'Department.Users.';
    }

    public function index()
    {
        return view($this->path . 'index');
    }

    public function create()
    {
        return view($this->path . 'create');
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), $this->rules(), $this->messages());

        if ($validate->fails()) {
            $text = "e:تاكد من البيانات";
//            dd($text);

            return response(['status' => false, 'errors' => $validate->messages(),'msg' => $text], 422);
        } else {
            try {

                $ads = User::create([
                    'name' => $request->name,
                    "email" => $request->email,
                    "password" => Hash::make($request->password),
                    'set_id' =>1,
                    'department_id' =>2,
                    'role_id' =>4,

                ]);

                $text = "s:تمت اضافة " . $this->title_ar . " بنجاح";
                return response(['status' => true, 'close' => 1, 'msg' => $text, 'title' => $this->title_ar]);
            } catch (QueryException $queryException) {
                $text = "e:لم يتم اضافة " . $this->title_ar . " بنجاح";
                return response(['status' => false, 'close' => 0, 'msg' => $text, 'title' => $this->title_ar]);
            }
        }

    }
    public function show($id)
    {
        //
    }

    public function edit(User $user)
    {
        return view($this->path . 'edit',[
            'user' => $user
        ]);
    }

    public function update(Request $request, User $user)
    {
        $validate = Validator::make($request->all(), $this->rules($user->id), $this->messages($user->id));

        if ($validate->fails()) {
            $text = "e:تاكد من البيانات";
            return response(['status' => false, 'errors' => $validate->messages(),'msg' => $text], 422);
        } else {
            try {

                $ads = $user->update([
                    'name' => $request->name,
                ]);

                $text = "s:تمت التعديل " . $this->title_ar . " بنجاح";
                return response(['status' => true, 'close' => 1, 'msg' => $text, 'title' => $this->title_ar]);
            } catch (QueryException $queryException) {
                $text = "e:لم يتم التعديل " . $this->title_ar . " بنجاح";
                return response(['status' => false, 'close' => 0, 'msg' => $text, 'title' => $this->title_ar]);
            }
        }
    }

    public function destroy(User $user)
    {
        try {

            $user->delete();
            $text = "s: تمت الحذف  " . $this->title_ar . " بنجاح";
            return response(['status' => true, 'close' => 1, 'msg' => $text, 'title' => $this->title_ar]);
        } catch (QueryException $queryException) {
            $text = "e:المستخدم غير موجود";
            return response(['status' => false, 'close' => 0, 'msg' => $text, 'title' => $this->title_ar]);
        }
    }



    private function rules($id = null, $request = null)
    {
        if($id)
        {
            $rules['name'] = 'required|min:3';

        }

        if(!$id)
        {
            $rules = [

                'name' => 'required|min:3',
                'email' => 'required|unique:users',

                'password' => 'required|min:3|confirmed',
                'password_confirmation' => 'required|min:3'
            ];
        }


        return $rules;

    }
    private function messages($id = null)
    {
       if(!$id){
           return [
               'name.required' => 'الاسم مطلوب',
               'name.min' => 'الاسم يجب ان يحتوي ع الاقل 3 احرف',
               'name.unique' => 'لقد ادخلت هذا الاسم مسبقا',


               'email.required' => 'الايميل مطلوب',
               'email.unique' => 'لقد ادخلت هذا الايميل مسبقا',

               'password.confirmed' => 'كلمتي السر غير متطابقتين',
               'password.required' => 'كلمة السر مطلوبة',
               'password_confirmation.required' => 'كلمة السر مطلوبة',
               'password.min' => 'كلمة السر اقل من 3 احرف',
               'password_confirmation.min' => 'كلمة السر اقل من 3 احرف'

           ];
       }else{
           return [
           'name.required' => 'الاسم مطلوب',
               'name.min' => 'الاسم يجب ان يحتوي ع الاقل 3 احرف',];
       }
    }

}


