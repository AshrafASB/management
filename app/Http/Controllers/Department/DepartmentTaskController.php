<?php

namespace App\Http\Controllers\Department;

use App\Http\Controllers\Controller;
use App\Task;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DepartmentTaskController extends Controller
{
    public function __construct()
    {

        $this->title_en = 'task';
        $this->title_ar = 'المهمة ';
        $this->path = 'Department.Task.';
    }

    public function index()
    {
        return view($this->path . 'index');
    }

    public function create()
    {
        return view($this->path . 'create');
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), $this->rules(), $this->messages());

        if ($validate->fails()) {
            $text = "e:تاكد من البيانات";
//            dd($text);

            return response(['status' => false, 'errors' => $validate->messages(), 'msg' => $text], 422);
        } else {
            try {
                $ads = Task::create([
                    'name' => $request->name,
                    "description" => $request->description,
                    "department_id" => self::getAuthDepartmentID(),
                    "user_id" => self::authId(),
                    'progress' =>null,
                    'IsDone' => false
                ]);

                $text = "s:تمت اضافة " . $this->title_ar . " بنجاح";
                return response(['status' => true, 'close' => 1, 'msg' => $text, 'title' => $this->title_ar]);
            } catch (QueryException $queryException) {
                $text = "e:لم يتم اضافة " . $this->title_ar . " بنجاح";
                return response(['status' => false, 'close' => 0, 'msg' => $text, 'title' => $this->title_ar]);
            }
        }
    }





    public function edit(Task $task)
    {
      return view($this->path . 'edit',[
          'task' => $task,
          'users'=>User::where('department_id','=',self::getAuthDepartmentID())->get()

      ]);
    }



    public function update(Request $request, Task $task)
    {
        $validate = Validator::make($request->all(), $this->rules($task->id), $this->messages($task->id));

        if ($validate->fails()) {
            $text = "e:تاكد من البيانات";
//            dd($text);

            return response(['status' => false, 'errors' => $validate->messages(), 'msg' => $text], 422);
        } else {
            try {

                $ads = $task->update([
                    'name' => $request->name,
                    "description" => $request->description,
                ]);

                $text = "s:تمت اضافة " . $this->title_ar . " بنجاح";
                return response(['status' => true, 'close' => 1, 'msg' => $text, 'title' => $this->title_ar]);
            } catch (QueryException $queryException) {
                $text = "e:لم يتم اضافة " . $this->title_ar . " بنجاح";
                return response(['status' => false, 'close' => 0, 'msg' => $text, 'title' => $this->title_ar]);
            }
        }
    }



    public function destroy(Task $task)
    {
        try {

            $task->delete();
            $text = "s: تمت الحذف  " . $this->title_ar . " بنجاح";
            return response(['status' => true, 'close' => 1, 'msg' => $text, 'title' => $this->title_ar]);
        } catch (QueryException $queryException) {
            $text = "e:المهمة غير موجود";
            return response(['status' => false, 'close' => 0, 'msg' => $text, 'title' => $this->title_ar]);
        }
    }

    private function rules($id = null)
    {
        $rules = [
            'name' => 'required|min:3',
            'description' => 'required',
        ];

        return $rules;

    }
    private function messages($id = null)
    {
        return [
            'name.required' => 'الاسم مطلوب',
            'name.min' => 'الاسم يجب ان يحتوي ع الاقل 3 احرف',

        ];
    }

}


