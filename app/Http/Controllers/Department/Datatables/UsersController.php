<?php

namespace App\Http\Controllers\Department\Datatables;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function getUsers(Request $request)
    {

        $data = User::with('department', 'set', 'role')
            ->where('department_id', self::getAuthDepartmentID())
            ->get()->map(function ($item) {
                $item->DepName = $item->department->name;
                $item->SetName = $item->set->name;
                $item->RoleName = $item->role->name;
                return $item;
            });

//        dd($data->first());
        return \DataTables::of($data)
            ->addColumn('Actions', function ($data) {
                $btn = '<a title="تعديل المستخدم"  href="' . route('department.user.edit', $data->id) . '"  class="PopUp btn btn-outline-primary btn-sm m-btn m-btn--icon  m-btn--icon-only m-btn--pill"><i class="flaticon-settings"></i></a>
                <a  title="حذف المستخدم"  tname="UsersTable" href="' . route('department.user.destroy', $data->id) . '" class="Confirm  btn btn-outline-danger btn-sm m-btn m-btn--icon  m-btn--icon-only m-btn--pill"><i class="flaticon-delete-1"></i></a>';
                return $btn;
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }
}
