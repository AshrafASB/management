<?php

namespace App\Http\Controllers\Department\Datatables;

use App\Http\Controllers\Controller;
use App\Task;
use Illuminate\Http\Request;

class TasksController extends Controller
{

    public function getTasks(Request $request)
    {
        $data = Task::with('department', 'achievement','user')
            ->where('department_id', self::getAuthDepartmentID())
            ->get()->map(function ($task){
                $task->username = $task->user->name;
                return $task;
            });

//        dd($data->first());
        return \DataTables::of($data)
            ->addColumn('Actions', function ($data) {
                $btn = '<a title="تعديل المهمة "  href="' . route('department.task.edit', $data->id) . '"  class="PopUp btn btn-outline-primary btn-sm m-btn m-btn--icon  m-btn--icon-only m-btn--pill"><i class="flaticon-settings"></i></a>
                <a  title="حذف المهمة"  tname="TaskTable" href="' . route('department.task.destroy', $data->id) . '" class="Confirm  btn btn-outline-danger btn-sm m-btn m-btn--icon  m-btn--icon-only m-btn--pill"><i class="flaticon-delete-1"></i></a>';
                return $btn;
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }
}
