<?php

namespace App\Http\Controllers\Department\Datatables;

use App\Achievement;
use App\Http\Controllers\Controller;
use App\Task;
use Illuminate\Http\Request;

class AchController extends Controller
{
    public function getAchs(Request $request)
    {
        $data = Achievement::with(['task.department.set', 'user'])
            ->get()->map(function ($item){
                $item->username = $item->user->name;
                $item->TaskName = $item->task->name;
                $item->TaskName = $item->task->name;
                $item->DepartmentName = $item->task->department->name;
                $item->SetName = $item->task->department->set->name;
                return $item;
            });

//        dd($data->first());
        return \DataTables::of($data)
            ->addColumn('Actions', function ($data) {
                $btn = '<a title="تعديل الانجاز "  href="' . route('department.ach.edit', $data->id) . '"  class="PopUp btn btn-outline-primary btn-sm m-btn m-btn--icon  m-btn--icon-only m-btn--pill"><i class="flaticon-settings"></i></a>
                <a  title="حذف الانجاز"  tname="AchTable" href="' . route('department.ach.destroy', $data->id) . '" class="Confirm  btn btn-outline-danger btn-sm m-btn m-btn--icon  m-btn--icon-only m-btn--pill"><i class="flaticon-delete-1"></i></a>';
                return $btn;
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }
}
