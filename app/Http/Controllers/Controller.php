<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
/*
    public static $data = [];

    public function __construct()
    {

        self::$data['active_menu'] = '';

    }*/

    public function authId()
    {
        return Auth::user()->id;
    }

    public function getAuthDepartmentID()
    {
        return Auth::user()->department_id;
    }

    public function getSetID()
    {
        return Auth::user()->set_id;
    }

    public function getRoleID()
    {
        return Auth::user()->role_id;
    }
}
