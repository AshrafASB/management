<?php

namespace App\Http\Controllers\Set;

use App\Department;
use App\Http\Controllers\Controller;
use App\Role;
use App\Set;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SetUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //هات كل الأشخاص الموجودين في الدائرة الي انا فيها
        $data['data'] = User::with('set')->where('set_id',self::getSetID())->get();
//        dd($data['data']);
        return view('set.user.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['role'] = Role::all()->pluck('name', 'id');
        $data['set'] = Set::all();
        $data['department'] = Department::all();
        return view('set.user.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'department_id' => $request->input('department_id'),
                'set_id' => self::getSetID(),
                'role_id' => $request->input('role_id')

            ]);
            return redirect()->route('set.user.index')
                ->with('success', 'تمت عملية الإضافة بنجاح');
        } catch (\Exception $exception) {
            return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $updateUser = User::with(['department','role'])->findOrFail($id);
            $data['role'] = Role::all()->pluck('name', 'id');
            $data['department'] = Department::all();
            return view('set.user.edit',$data)->with('updateUser',$updateUser);
        }catch (\Exception $exception){
            dd($exception);
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:50',
            'email' => 'email',
            'password'=>'required|confirmed',
            'password_confirmation'=>'sometimes|required_with:password',
        ]);

        try {
            $updateUser = User::findOrFail($id);
            $updateUser->name = $request->input('name');
            $updateUser->email = $request->input('email');
            $updateUser->password =  Hash::make($request['password']);
            $updateUser->set_id = self::getSetID();
            $updateUser->role_id = $request->input('role_id');
            $updateUser->department_id = $request->input('department_id');
            $updateUser->update();
            return redirect()->route('set.user.index')
                ->with('success',"تم تحدسث بيانات المستخدم بنجاح");
        }catch (\Exception $exception){
            return redirect()->route('set.user.edit',[$id])->with('error','حدث خطأ أثناء عملية النعديل '.$exception);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $delteUser =  User::findOrFail($id);
            $delteUser->delete();
            return redirect()->route('set.user.index')->with('success',"Deleted [ $delteUser->name ] Successful");

        }catch (\Exception $exception){
            return redirect()->route('set.user.index')->with('error','حدث خطأ أثناء عملية الحذف:'.$exception);
        }
    }
}
