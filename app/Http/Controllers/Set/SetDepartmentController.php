<?php

namespace App\Http\Controllers\Set;

use App\Department;
use App\Http\Controllers\Controller;
use App\Set;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SetDepartmentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['data'] = Department::with('set')->where('set_id',self::getSetID())->get();
        return view('set.department.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('set.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

            try {
            Department::create([
                'name'=>$request->input('name'),
                'description'=>$request->input('description'),
                'set_id'=>self::getSetID(),
            ]);
            return redirect()->route('set.department.index')
                ->with('success', 'تمت عملية الإضافة بنجاح');
        } catch (\Exception $exception) {
            dd($request);
            return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
//        $item = Department::findOrFail($id);
//        return view('department.edit')->with('id',$item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $depId = Department::findOrFail($id);
            return view('set.department.edit')->with('depId',$depId);
        }catch (\Exception $exception){
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {

        try {
            $updateDep = Department::findOrFail($id);
            $updateDep->name = $request->input('name');
            $updateDep->description = $request->input('description');

            $updateDep->update();
            return redirect()->route('set.department.index')
                ->with('success',"Department $updateDep->name OR $updateDep->description updated Successfully");
        }catch (\Exception $exception){
            return redirect()->route('set.department.edit',[$id])->with('error','Update Fail');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
//        dd(1);
        try {
            $deleteID = Department::findOrFail($id);
            $deleteID->delete();
            return redirect()->route('set.department.index')->with('success', "تم حذف القسم بنجاح");
        }catch (\Exception $exception){
//            dd(1);
            return redirect()->route('$deleteID')->with('error','عذرا !! هناك خطأ في عملية الحذف'.$exception);
        }

    }
}
