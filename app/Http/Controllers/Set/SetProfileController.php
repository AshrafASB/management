<?php

namespace App\Http\Controllers\Set;

use App\Http\Controllers\Controller;
use App\Profile;
use App\User;
use App\UserInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SetProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['item'] = User::with(['information', 'communication'])->find(Auth::user()->id);

        return view('set.profile.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function imageUploadPost(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time() . '.' . $request->image->extension();

        $request->image->move(public_path('images'), $imageName);

        return back()
            ->with('success', 'You have successfully upload image.')
            ->with('image', $imageName);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $user = Profile::where('user_id', Auth::user()->id)->first();
        if ($user) {
            // dd(UserInformation::whereId($user->id)->update($request->all())->toSql());

            try {
                UserInformation::whereId($user->id)->update(['id_number' => $request['id_number'],
                    'f_name' => $request['f_name'],
                    'l_name' => $request['l_name'],
                    'DOB' => $request['DOB'],
                    'POB' => $request['POB'],
                    'user_id' => Auth::user()->id]);
                return redirect()->route('set.profile.index')
                    ->with('success', 'تمت عملية الإضافة بنجاح');
            } catch (\Exception $exception) {
                return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
            }
        } else {
            try {
                //UserInformation::create($request->all());
                UserInformation::create([
                    'id_number' => $request['id_number'],
                    'f_name' => $request['f_name'],
                    'l_name' => $request['l_name'],
                    'DOB' => $request['DOB'],
                    'POB' => $request['POB'],
                    'user_id' => Auth::user()->id,
                ]);
                return redirect()->route('set.profile.index')
                    ->with('success', 'تمت عملية الإضافة بنجاح');
            } catch (\Exception $exception) {
                return back()->with('danger', 'عذراً: حدث خلل أثناء الإرسال، حاول في وقت آخر' . $exception->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
