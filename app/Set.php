<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Set extends Model
{
    protected $table = "sets";
    protected $fillable = [
        'id',
        'name',
        'description',
    ];

    public function department()
    {
        return $this->hasMany(Department::class, 'set_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
