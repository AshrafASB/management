<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserInformation extends Model
{
    protected $table = "user_informations";
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'id_number',
        'f_name',
        'l_name',
        'DOB',
        'POB',
        'user_id',
        'created_at',
        'updated_at',
    ];

   public function user()
    {
        return $this->belongsTo(User::class);
    }
}
