<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = "departments";
    protected $fillable = [
        'id',
        'name',
        'description',
        'set_id',
    ];
    protected $dates = ['created_at','updated_at'];


    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function set()
    {
        return $this->belongsTo(Set::class,'set_id');
    }

    public function task()
    {
        return $this->hasMany(Task::class);
    }
}
