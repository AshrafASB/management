<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;
    protected $table = "massages";
    protected $fillable = [
        'from',
        'to',
        'title',
        'description',
        'isRead',
        'spam',
        'isStarred',
    ];
//    protected $with = ['users'];
    protected $dates = ['created_at', 'updated_at'];


    public function send()
    {
        return $this->belongsTo(User::class,'from');
    }

    public function res()
    {
        return $this->belongsTo(User::class,'to');
    }
}
