<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

   protected $table = 'users';
    protected $fillable = [
        'name', 'email', 'password', 'department_id', 'set_id', 'role_id','password_confirmation'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasRole($role)
    {
        $roleName = Role::find($role)->select('name');
        return $this->role == $roleName;
    }

    public function tasks()
    {
        return $this->hasMany(Task::class, 'user_id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function set()
    {
        return $this->belongsTo(Set::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function information()
    {
        return $this->hasOne(UserInformation::class);
    }

    public function communication()
    {
        return $this->hasMany(UserCommunication::class);
    }

    public function medias()
    {
        return $this->hasMany(Media::class);
    }
   public function messages()
    {
        return $this->hasMany(Massage::class);
    }


//

}
