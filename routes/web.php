<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');

    dd('success');
});

Route::redirect('/', 'login');

Auth::routes();

Route::get('test', 'testController@index');


Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth', 'admin']], function () {
    Route::resource('messages', 'AdminMessageController');
    Route::resource('department', 'AdminDepartmentController');
    Route::resource('user', 'AdminUserController');
    Route::resource('task', 'AdminTaskController');
    Route::resource('set', 'AdminSetController');
    Route::resource('role', 'AdminRoleController');
    Route::resource('achievement', 'AdminAchievementsController');
    Route::resource('profile', 'AdminProfileController');
    Route::post('image-upload', 'AdminProfileController@imageUploadPost')->name('image.upload.post');

    //Route::get('profile', 'AdminController@editProfile')->name('profile');
    // Route::get('profile', 'AdminController@editProfile')->name('profile');
    Route::put('profile', 'AdminController@updateProfile')->name('profile.update');
    Route::put('password', 'AdminController@updatePassword')->name('profile.password');

});


//////////////////////////// Last Update /////////////////////////////
Route::group(['prefix' => 'set', 'as' => 'set.', 'namespace' => 'Set', 'middleware' => ['auth', 'sets']], function () {

    Route::resource('department', 'SetDepartmentController');
    Route::resource('user', 'SetUserController');
    Route::resource('profile', 'SetProfileController');
    Route::post('image-upload', 'SetProfileController@imageUploadPost')->name('image.upload.post');

//////////////////////////// Last Update /////////////////////////////

//    Route::resource('task', 'SectionTaskController');
//    Route::resource('user', 'SectionUserController');
//    Route::resource('collection', 'SectionCollectionController');
//    Route::get('profile', 'DepartmentController@editProfile')->name('profile');
//    Route::put('profile', 'DepartmentController@updateProfile')->name('profile.update');
//    Route::put('password', 'DepartmentController@updatePassword')->name('profile.password');
});


Route::group(['prefix' => 'department', 'as' => 'department.', 'namespace' => 'Department', 'middleware' => ['auth', 'department']], function () {
//    Route::resource('collection', 'DepartmentCollectionController');
//    Route::resource('section', 'DepartmentSectionController');
    Route::get('home', function () {
        return view('Department.home');
    })->name('home');
    Route::resource('user', 'DepartmentUserController');
    Route::resource('task', 'DepartmentTaskController');
    Route::resource('ach', 'DepartmentAchController');
    Route::get('profile', 'ProfileController@index')->name('profile');
//    Route::put('profile', 'DepartmentController@updateProfile')->name('profile.update');
//    Route::put('password', 'DepartmentController@updatePassword')->name('profile.password');
    //Datatables
    Route::get('getUsers', 'Datatables\UsersController@getUsers')->name('getUsers');
    Route::get('getTasks', 'Datatables\TasksController@getTasks')->name('getTasks');
    Route::get('getAchs', 'Datatables\AchController@getAchs')->name('getAchs');

});

//////////////////////////// Last Update /////////////////////////////
Route::group(['prefix' => 'user', 'as' => 'user.', 'namespace' => 'User', 'middleware' => ['auth', 'user']], function () {

    // Route::resource('department', 'SetDepartmentController');
    //Route::resource('user', 'SetUserController');
    Route::resource('task', 'UserTasksController');
   // Route::get('task','UserTasksController@index');
    Route::resource('profile', 'UserProfileController');
    Route::resource('achievement', 'UserAchievementsController');
    Route::post('image-upload', 'UserProfileController@imageUploadPost')->name('image.upload.post');

//////////////////////////// Last Update /////////////////////////////

//    Route::resource('task', 'SectionTaskController');
//    Route::resource('user', 'SectionUserController');
//    Route::resource('collection', 'SectionCollectionController');
//    Route::get('profile', 'DepartmentController@editProfile')->name('profile');
//    Route::put('profile', 'DepartmentController@updateProfile')->name('profile.update');
//    Route::put('password', 'DepartmentController@updatePassword')->name('profile.password');
});

Route::get('/home', 'Controller@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/logout', function () {
    Auth::logout();
    return redirect('/login');
})->name('home.loguot');


