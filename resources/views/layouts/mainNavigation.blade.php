<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true"
        data-slide-speed="200">
        {{--Start Admin Links--}}

        {{--End Admin Links--}}
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

            <!-- Main -->
            <li class="active">
                <a href="{{ url('/') }}">
                    <i class="icon-home4"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            @if(Auth::user()->role->name == 'user')
                <li class="nav-item start">
                    <a href="{{route('user.achievement.index')}}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">الانجاز اليومي </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('user.task.index')}}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title"> المهام </span>
                    </a>
                </li>
            @endif

            @if(Auth::user()->role->name == 'set')
                <li class="nav-item start">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">الانجاز اليومي </span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('set.department.index')}}" class="nav-link nav-toggle">
                        <i class="icon-users2"></i>
                        <span class="title">الأقسام</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="{{route('set.user.index')}}" class="nav-link nav-toggle">
                        <i class="icon-users"></i>
                        <span class="title">المستخدمين</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.task.index')}}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title"> المهام </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.task.index')}}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title"> الإنجازات </span>
                    </a>
                </li>
            @endif
            {{--end Set Section in Nav--}}


            {{--Start Admin Links--}}
            @if(Auth::user()->role->name == 'department')
                <li class="nav-item  ">
                    <a href="{{route('department.section.index')}}" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">الأقسام</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="{{route('department.user.index')}}" class="nav-link nav-toggle">
                        <i class="icon-users"></i>
                        <span class="title">المستخدمين</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('department.task.index')}}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title"> الإنجازات </span>
                    </a>
                </li>

            @endif
            @if(Auth::user()->role->name == 'admin')

                <li class="nav-item  ">
                    <a href="{{route('admin.set.index')}}" class="nav-link nav-toggle">
                        <i class="icon-users"></i>
                        <span class="title">الدوائر</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="{{route('admin.department.index')}}" class="nav-link nav-toggle">
                        <i class="icon-users2"></i>
                        <span class="title">الأقسام</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="{{route('admin.user.index')}}" class="nav-link nav-toggle">
                        <i class="icon-users"></i>
                        <span class="title">المستخدمين</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('admin.task.index')}}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title"> المهام </span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('admin.achievement.index')}}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title"> الإنجازات </span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('admin.role.index')}}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title"> الصلاحيات </span>
                    </a>
                </li>

            @endif

        </ul>
    </div>
</div>
<!-- /main navigation -->
