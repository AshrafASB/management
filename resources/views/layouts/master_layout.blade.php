<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta charset="utf-8" />
    <title>CMS</title>
    <meta name="description" content="Blank inner page examples">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">



    {{-- Left to right --}}
    {{-- <link href="{{asset('libraries/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('libraries/assets/demo/demo12/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />--}}



    {{-- Right to left --}}
    <link href="{{asset('libraries/assets/vendors/base/vendors.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('libraries/assets/demo/demo12/base/style.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />






    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" />







    <!-- <link rel="shortcut icon" href="~/assets/demo/demo12/media/img/logo/favicon.ico" /> -->

    <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
    <style>
        body {
            font-family: 'Cairo', sans-serif;
        }

        .pagination {
            float: left !important;
        }
    </style>

    @yield('css')
</head>

<!-- end::Head -->
<!-- begin::Body -->

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">

        <!-- BEGIN: Header -->
        <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
            <div class="m-container m-container--fluid m-container--full-height">
                <div class="m-stack m-stack--ver m-stack--desktop">
                    <!-- BEGIN: Brand -->
                    <div class="m-stack__item m-brand  m-brand--skin-dark ">
                        <div class="m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                <a href="index.html" class="m-brand__logo-wrapper">
                                    <img alt="" src="{{asset('libraries/assets/demo/demo12/media/img/logo/logo.png')}}" />
                                </a>
                            </div>
                            <div class="m-stack__item m-stack__item--middle m-brand__tools">

                                <!-- BEGIN: Left Aside Minimize Toggle -->
                                <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
                                    <span></span>
                                </a>

                                <!-- END -->
                                <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                                <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                    <span></span>
                                </a>

                                <!-- END -->
                                <!-- BEGIN: Responsive Header Menu Toggler -->
                                <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                    <span></span>
                                </a>

                                <!-- END -->
                                <!-- BEGIN: Topbar Toggler -->
                                <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                    <i class="flaticon-more"></i>
                                </a>

                                <!-- BEGIN: Topbar Toggler -->
                            </div>
                        </div>
                    </div>

                    <!-- END: Brand -->
                    <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

                        <!-- BEGIN: Horizontal Menu -->
                        <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                        <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">

                        </div>

                        <!-- END: Horizontal Menu -->
                        <!-- BEGIN: Topbar -->
                        <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-topbar__nav-wrapper">
                                <ul class="m-topbar__nav m-nav m-nav--inline">

                                    <li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                                        <a href="#" class="m-nav__link m-dropdown__toggle">
                                            <span class="m-topbar__userpic">
                                                <img src="{{asset('libraries/assets/app/media/img/users/user4.jpg')}}" class="m--img-rounded m--marginless m--img-centered" alt="" />
                                            </span>
                                            <span class="m-nav__link-icon m-topbar__usericon  m--hide">
                                                <span class="m-nav__link-icon-wrapper"><i class="flaticon-user-ok"></i></span>
                                            </span>
                                            <span class="m-topbar__username m--hide">Ahmed Shalayel</span>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__header m--align-center">
                                                    <div class="m-card-user m-card-user--skin-light">
                                                        <div class="m-card-user__pic">
                                                            <img src="{{asset('libraries/assets/app/media/img/users/user4.jpg')}}" class="m--img-rounded m--marginless" alt="" />
                                                        </div>
                                                        <div class="m-card-user__details">
                                                            <span class="m-card-user__name m--font-weight-500">Ahmed Shalayel</span>
                                                            <a href="" class="m-card-user__email m--font-weight-300 m-link">eng.ahmedkhshalayel@gmail.com</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav m-nav--skin-light">

                                                            <li class="m-nav__item">
                                                                <a href="snippets/pages/user/login-1.html" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>

                        <!-- END: Topbar -->
                    </div>
                </div>
            </div>
        </header>

        <!-- END: Header -->
        <!-- begin::Body -->
        <div style="padding-top:51px !important" class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
            <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

                <!-- BEGIN: Aside Menu -->
                <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
                    <ul class="m-menu__nav ">

                        <li class="m-menu__item " aria-haspopup="true"><a href="index.html" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">الرئيسية</span></a></li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-layers"></i><span class="m-menu__link-text">ادارة الاخبار</span><i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">

                                    <li class="m-menu__item " aria-haspopup="true"><a href="~/Admin/Post" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">المقالات</span></a></li>

                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-layers"></i><span class="m-menu__link-text">ادارة المستخدمين</span><i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">

                                    <li class="m-menu__item " aria-haspopup="true"><a href="~/Admin/User" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">المستخدمين</span></a></li>

                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-layers"></i><span class="m-menu__link-text">ادارة التعليقات</span><i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">

                                    <li class="m-menu__item " aria-haspopup="true"><a href="~/Admin/Comment" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">التعليقات</span></a></li>

                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-layers"></i><span class="m-menu__link-text">ادارة الاعلانات</span><i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">

                                    <li class="m-menu__item " aria-haspopup="true"><a href="" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">الاعلانات</span></a></li>

                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-layers"></i><span class="m-menu__link-text">معارض الصور</span><i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">

                                    <li class="m-menu__item " aria-haspopup="true"><a href="" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">معارض الصور</span></a></li>

                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-layers"></i><span class="m-menu__link-text">ادارة التصنيفات</span><i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">

                                    <li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text"> التصنيفات</span></a></li>

                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>

                <!-- END: Aside Menu -->
            </div>

            <!-- END: Left Aside -->
            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <div class="m-content">
                    @yield('content')

                </div>
            </div>
        </div>

        <!-- end:: Body -->
        <!-- begin::Footer -->
        <footer class="m-grid__item		m-footer ">
            <div class="m-container m-container--fluid m-container--full-height m-page__container">
                <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                    <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                        <span class="m-footer__copyright">
                            2019 &copy; CMS <a href="https://keenthemes.com" class="m-link">hi</a>
                        </span>
                    </div>

                </div>
            </div>
        </footer>

        <!-- end::Footer -->
    </div>


    <div id="m_scroll_top" class="m-scroll-top">
        <i class="la la-arrow-up"></i>
    </div>
    <div class="modal fade" id="PopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">


                <div class="modal-header">
                    <h5 class="modal-title"></h5>

                    <div data-dismiss="modal" class="m-demo-icon__preview">
                        <i style="color:white" class="la la-close"></i>
                    </div>

                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="Confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Warning !</h5>
                </div>
                <div class="modal-body">
                    <p>Are You Shore ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" data-action="delete-user">Yes</button>
                </div>
            </div>
        </div>
    </div>


    <script src="{{asset('libraries/nprogress-master/nprogress.js')}}"></script>

    <script src="{{asset('libraries/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('libraries/assets/demo/demo12/base/scripts.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('libraries/assets/demo/demo12/base/datatables/basic.js')}}" type="text/javascript"></script>
    <script src="{{asset('libraries/assets/demo/demo12/base/datatables/datatables.bundle.js')}}" type="text/javascript"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script>
        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }

        $(".ajaxForm").ajaxForm({
            success: function(json) {
                if (json.status == 1) {

                    var tname = $('.ajaxForm').attr("tname");
                    var fname = $('.ajaxForm').attr("fname");

                    if (tname != null) {
                        RefreshData(tname);
                    }
                    if (fname != null) {
                        eval(fname);
                    }

                    if (!$("#tblItems").hasClass("autohide")) {
                        $(".ajaxForm").resetForm();
                        $("#tblItems tbody tr").remove();
                        $("#tblItems").addClass("hidden").next().show();

                    } else {
                        $(".select2").val('').change();
                    }
                }
                console.log(json.close);
                if (json.msg != null && json.title != null)
                    ShowMessage(json.msg, json.title);

                if (json.redirect != null)
                    window.location = json.redirect;
                if (json.close == 1)
                    $("#PopUp").modal("hide");
                $(".ajaxForm :submit").prop("disabled", false);
            },
            beforeSubmit: function() {
                $(".ajaxForm :submit").prop("disabled", true);
            }
        });


        $(document).on("click", ".PopUp", function() {
            if ($(this).attr("href") != '') {
                $("#PopUp").modal("show");
                $("#PopUp .modal-body").html('<h3 class="text-center text-danger"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></h3>');
                $("#PopUp .modal-body").load($(this).attr("href"), function() {
                    setTimeout(function() {
                        $("#PopUp [autofocus]").focus().select();
                    }, 500);
                });
                $("#PopUp .modal-title").text($(this).attr("title"));
                return false;
            }
        });


        $(document).on("click", ".Confirm", function() {
            $("#Confirm").modal("show");
            $("#Confirm .btn-danger").attr("href", $(this).attr("href"));
            $("#Confirm .btn-danger").attr("tname", $(this).attr("tname"));
            return false;
        });


        $("#Confirm .btn-danger").click(function() {
            var tname = $(this).attr("tname");
            var fname = $('.ajaxForm').attr("fname");
            var url = $(this).attr("href");
            var request = $.ajax({
                type: 'post',
                url: url,
                success: function(json) {
                    if (json.status == 1) {
                        if (tname != null) {
                            RefreshData(tname);
                        }
                        if (fname != null) {
                            eval(fname);
                        }
                    }
                    ShowMessage(json.msg, json.title);
                }
            });
            request.fail(function(response, exception) {
                // http.fail(JSON.parse(response.responseText), true);
                alert(response.responseText);
            });
            $("#Confirm").modal("hide");
            return false;
        });

        function RefreshData(tn) {
            var xtable = $("#" + tn).dataTable();
            var info = $("#" + tn).DataTable().page.info();
            xtable.fnDraw();
            xtable.fnPageChange(info.page);
        }


        $(document).ajaxStart(function() {
            $(".wait").text("Wait...");
            NProgress.start();
        });
        $(document).ajaxStop(function() {
            $(".wait").text("");
            NProgress.done();
        });
        $(document).ajaxError(function() {
            $(".wait").text("");
            NProgress.done();
            $(".ajaxForm :submit").prop("disabled", false);
        });


        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        function ShowMessage(msg, title = "defualt") {
            var cls = "info";
            if (msg.indexOf("s:") == 0) {
                cls = "success";
                msg = msg.substring(2);
            }
            if (msg.indexOf("w:") == 0) {
                cls = "warning";
                msg = msg.substring(2);
            }
            if (msg.indexOf("e:") == 0) {
                cls = "error";
                msg = msg.substring(2);
            }
            if (msg.indexOf("i:") == 0) {
                cls = "info";
                msg = msg.substring(2);
            }
            toastr[cls](msg, title);
        }

        function PageLoadActions() {
            $(".date").datepicker({
                format: "dd/mm/yyyy",
                endDate: "+0d",
                autoclose: true
            });

            $(".input-daterange").datepicker({
                format: "dd/mm/yyyy",
                endDate: "+0d",
                autoclose: true
            });

            $(".ajaxForm").ajaxForm({
                success: function(json) {
                    if (json.status == 1) {

                        var tname = $('.ajaxForm').attr("tname");
                        var fname = $('.ajaxForm').attr("fname");

                        if (tname != null) {
                            RefreshData(tname);
                        }
                        if (fname != null) {
                            eval(fname);
                        }

                        if (!$("#tblItems").hasClass("autohide")) {
                            $(".ajaxForm").resetForm();
                            $("#tblItems tbody tr").remove();
                            $("#tblItems").addClass("hidden").next().show();

                        } else {
                            $(".select2").val('').change();
                        }
                    }
                    console.log(json.close);
                    if (json.msg != null)
                        ShowMessage(json.msg, json.title);

                    if (json.redirect != null)
                        window.location = json.redirect;
                    if (json.close == 1)
                        $("#PopUp").modal("hide");
                    $(".ajaxForm :submit").prop("disabled", false);
                },
                beforeSubmit: function() {
                    $(".ajaxForm :submit").prop("disabled", true);
                },
                // if we get error we'll do these things =>
                error: function(err) {
                    // first convert the error text to json object
                    var listErr = JSON.parse(err.responseText);
                    // then check if we get the same status code that we have sent in previous
                    if (err.status == 422) {
                        // get all span tags
                        var fields = document.querySelectorAll('.error');
                        // then loop throw them
                        for (const element of fields) {
                            // get each span name
                            var name = element.getAttribute("name")
                            // then trim the first charcater like this=>
                            var name = name.substring(1);
                            console.log(listErr);
                            // now we want to check if this input have an error or not
                            if(listErr.errors[name]){
// if so we will do this
                            element.textContent = listErr.errors[name][0]
                            }else{
                                // if not we will asssign an empty string like this=>
                                element.textContent = "";
                            }
                        }
                        // in the end will show an error msg like this =>
                        ShowMessage(JSON.parse(err.responseText).msg, 'خطأ');
                    }
                }
            });
        }







function RefreshGallery(data)
{



}

function processJson(data){
    console.log(data)
    RefreshGallery(data);
}


        function updateGallery() {

            $(".GalleryForm").ajaxForm({
                success: processJson ,
                beforeSubmit: function() {
                    $(".GalleryForm :submit").prop("disabled", true);
                },
                // if we get error we'll do these things =>
                error: function(err) {
                    // first convert the error text to json object
                    var listErr = JSON.parse(err.responseText);
                    // then check if we get the same status code that we have sent in previous
                    if (err.status == 422) {
                        // get all span tags
                        var fields = document.querySelectorAll('.error');
                        // then loop throw them
                        for (const element of fields) {
                            // get each span name
                            var name = element.getAttribute("name")
                            // then trim the first charcater like this=>
                            var name = name.substring(1);
                            console.log(listErr);
                            // now we want to check if this input have an error or not
                            if(listErr.errors[name]){
// if so we will do this
                            element.textContent = listErr.errors[name][0]
                            }else{
                                // if not we will asssign an empty string like this=>
                                element.textContent = "";
                            }
                        }
                        $(".GalleryForm :submit").prop("disabled", false);

                        // in the end will show an error msg like this =>
                        ShowMessage(JSON.parse(err.responseText).msg, 'خطأ');
                    }
                }
            });

        }






    </script>


    @yield('script')
</body>

<!-- end::Body -->

</html>
