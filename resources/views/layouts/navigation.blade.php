@if(Auth::user()->role->name == 'user')
    <li><a href="{{route('user.profile.index')}}"><i class="icon-user-plus"></i> <span>الصفحة الشخصية</span></a></li>
    <li><a href="#"><i class="icon-coins"></i> <span>My balance</span></a></li>
    <li><a href="{{route('admin.messages.index')}}"><i class="icon-comment-discussion"></i> <span><span
                        class="badge bg-teal-400 pull-right">58</span> الرسائل</span></a></li>
    <li class="divider"></li>
    <li><a href="#"><i class="icon-cog5"></i> <span>إعدادات الحساب</span></a></li>

@endif

@if(Auth::user()->role->name == 'set')
    {{--    {{route('set.profile.index')}}--}}
    <li><a href="{{route('set.profile.index')}}"><i class="icon-user-plus"></i> <span>الصفحة الشخصية</span></a></li>
    <li><a href="#"><i class="icon-coins"></i> <span>My balance</span></a></li>
    <li>
        <a href="#"><i class="icon-comment-discussion"></i> <span>
            <span class="badge bg-teal-400 pull-right">58</span> الرسائل</span>
        </a>
    </li>
    <li class="divider"></li>
    <li><a href="#"><i class="icon-cog5"></i> <span>إعدادات الحساب</span></a></li>

@endif

{{--Start Admin Links--}}
@if(Auth::user()->role->name == 'department')
    <li><a href="{{route('department.profile.index')}}"><i class="icon-user-plus"></i> <span>الصفحة الشخصية</span></a></li>
    <li><a href="#"><i class="icon-coins"></i> <span>My balance</span></a></li>
    <li><a href="#"><i class="icon-comment-discussion"></i> <span><span
                        class="badge bg-teal-400 pull-right">58</span> الرسائل</span></a></li>
    <li class="divider"></li>
    <li><a href="#"><i class="icon-cog5"></i> <span>إعدادات الحساب</span></a></li>


@endif

@if(Auth::user()->role->name == 'admin')

    <li><a href="{{route('admin.profile.index')}}"><i class="icon-user-plus"></i> <span>الصفحة الشخصية</span></a></li>
    <li><a href="#"><i class="icon-coins"></i> <span>My balance</span></a></li>
    <li><a href="{{route('admin.messages.index')}}"><i class="icon-comment-discussion"></i> <span><span
                    class="badge bg-teal-400 pull-right">58</span> الرسائل</span></a></li>
    <li class="divider"></li>
    <li><a href="#"><i class="icon-cog5"></i> <span>إعدادات الحساب</span></a></li>

@endif
