<!DOCTYPE html>
<html lang="ar" dir="rtl">
<head>

    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html" charset="UTF-8"/>
    <title>@yield('title')</title>

    <base href="http://localhost/management/public/">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <!-- Global stylesheets -->
    <link rel="stylesheet" type="text/css" href="{{asset('global_assets/css/icons/icomoon/styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/core.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/components.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/colors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/font.css')}}"/>
    <!-- /global stylesheets -->


    <!-- Core JS files -->
    <script src="{{asset('global_assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script src="{{asset('global_assets/js/core/libraries/jquery.min.js') }}"></script>
    <script src="{{asset('global_assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script src="{{asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{asset('assets/js/app.js') }}"></script>
    <script src="{{asset('global_assets/js/demo_pages/login.js') }}"></script>
    <script src="{{asset('global_assets/js/plugins/ui/ripple.min.js') }}"></script>
    <!-- /theme JS files -->
    @yield('css')
    @yield('js')
</head>
<body style="font-family: Roboto">
<!-- Main navbar -->
<div class="navbar navbar-default header-highlight">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ url('/') }}"><img src="global_assets/images/logo_light.png" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>

        <div class="navbar-right">
            <p class="navbar-text">Morning, {{ Auth::user()->name}}!</p>
            <p class="navbar-text"><span class="label bg-success">Online</span></p>
        </div>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">
                <!-- User menu -->
                <div class="sidebar-user-material">
                    <div class="category-content">
                        <div class="sidebar-user-material-content">
                            <a href="{{ url('/') }}">
                                <img src="global_assets/images/placeholders/placeholder.jpg"
                                     class="img-circle img-responsive" alt="">
                            </a>
                            <h6>{{ Auth::user()->name}}</h6>
                            <span class="text-size-small">
                                {{ Auth::user()->email}}
                            </span>
                        </div>

                        <div class="sidebar-user-material-menu">
                            <a href="#user-nav" data-toggle="collapse">
                                <span>{{"حسابي"}}</span>
                                <i class="caret"></i>
                            </a>
                        </div>
                    </div>

                    <div class="navigation-wrapper collapse" id="user-nav">
                        <ul class="navigation">
                            @include('layouts.navigation')
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="icon-switch2"></i>
                                    <span> {{ __('تسجيل الخروج') }}</span>
                                </a>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </ul>
                    </div>
                </div>
                <!-- /user menu -->
                @include('layouts.mainNavigation')
            </div>
        </div>
        <!-- /main sidebar -->
        <div class="content-wrapper">
            @yield('content')
        </div>
    </div>
    <!-- /page content -->
</div>
<!-- /page container -->
</body>
</html>
