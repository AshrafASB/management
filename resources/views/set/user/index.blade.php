@extends("layouts.public")

@section("title")
    المستخدمين
@endsection()

@section("css")

@endsection()

@section("content")

    <div class="content">
        <div class="tab-content">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h1 class="text-center" style="font-family: 'Al-Jazeera'">
                        {{"إدارة المستخدمين"}}
                    </h1>
                    <div class="heading-elements">
                        <a class="btn btn-success" href="{{ route('set.user.create') }}">{{" إضافة مستخدم جديد"}}</a>
                    </div>
                </div>

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="row" style="font-size: initial">
                    <div class="col-md-12">
                        <table class="table datatable-basic">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>الاسم</th>
                                <th>الايميل</th>
                                <th>الدائرة</th>
                                <th>القسم</th>
                                <th>الصلاحية</th>
                                <th>تاريخ</th>
                                <th>الحدث</th>
                            </tr>
                            </thead>
                            @foreach ($data as $key => $item)

                            <tr>
                                    <td>{{$item->id }}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->set->name }}</td>
                                    <td>{{ $item->department->name }}</td>
                                    <td>{{ $item->role->name }}</td>
                                    <td>{{ date('d-m-Y', strtotime($item->created_at)) }} </td>
                                    <td style="width: 22%">
                                        <a class="edit-modal btn btn-info"
                                           href="{{ route('set.user.edit',$item->id) }}">
                                            <i class="glyphicon glyphicon-edit"></i> تعديل
                                        </a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['set.user.destroy', $item->id],'style'=>'display:inline']) !!}
                                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i> حذف', ['type' => 'submit','class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="col-lg-10">
            <form method="get" action="{{--{{url('admin/category')}}--}}" class="row">
                <div class="col-sm-3">
                    <input autofocus name="q" type="text" value="{{--{{$q}}--}}" class="form-control"
                           placeholder="بحث عن مستخدم...">
                </div>

                <div class="col-sm-2">
                    <select class="form-control" name="is_active">
                        <option value=""> جميع الأقسام</option>
                        <option value="1"> ويب</option>
                        <option value="0"> اندرويد</option>
                    </select>
                </div>


                <span class="col-sm-1">
                    <button class="btn btn-primary" type="submit">بحث!</button>
                  </span>
            </form><!-- /input-group -->
        </div>
        <div class="col-lg-2 text-right"><a class="btn btn-success" href="{{route('admin.user.create')}}"> إضافة مستخدم
                جديد</a></div>
    </div>

@endsection()

@section("js")
    <!-- Theme JS files -->
    <script src="{{asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script src="{{asset('global_assets/js/demo_pages/datatables_basic.js') }}"></script>
    <!-- /theme JS files -->
@endsection()
