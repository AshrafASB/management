@extends("layouts.public")

@section("title")
    اضافة مستخدم جديد
@endsection()

@section("css")

@endsection()

@section("content")
    <div class="content">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div role="dialog">
            <div class="modal-dialog" style="width: 80% ;font-size: larger">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row">
                            <div class="col-lg-12 margin-tb">
                                <div class="pull-left">
                                    <h1 class="text-center" style="font-family: 'Al-Jazeera'">
                                        {{"إضافة مستخدم جديد"}}
                                    </h1>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-primary" href="{{ route('admin.user.index') }}"> رجوع</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        {!!Form::open(array('route' => 'admin.user.store','method'=>'POST','class'=>'form-horizontal','role'=>'form')) !!}
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="title">اسم المستخدم</label>
                            <div class="col-sm-9">
                                {!! Form::text('name', null, array('placeholder' => 'اسم المستخدم','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="title">البريد الإلكتروني</label>
                            <div class="col-sm-9">
                                {!! Form::text('email', null, array('placeholder' => 'البريد الإلكتروني','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="title">كلمة المرور</label>
                            <div class="col-sm-9">
                                {!! Form::password('password', array('placeholder' => 'كلمة المرور','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="title">تأكيد كلمة المرور</label>
                            <div class="col-sm-9">
                                {!! Form::password('confirm-password', array('placeholder' => 'تأكيد كلمة المرور','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fullname" class="col-sm-3 control-label">الدائرة </label>
                            <div class="col-sm-9">
                                <select class="form-control" name="set_id" required>
                                    <option value="">-- اختر الدائرة --</option>
                                    @foreach($set as $item)
                                        <option value="{{$item->id}}"> {{$item->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fullname" class="col-sm-3 control-label">القسم </label>
                            <div class="col-sm-9">
                                <select class="form-control" name="department_id" required>
                                    <option value="">-- اختر القسم --</option>
                                    @foreach($department as $item)
                                        <option value="{{$item->id}}"> {{$item->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fullname" class="col-sm-3 control-label">الصلاحية </label>
                            <div class="col-sm-9">
                                {!!Form::select('role_id',$role, null, ['placeholder' => '-- إختر الصلاحية --','class'=>'form-control'])!!}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success add">
                                <span class='glyphicon glyphicon-check'></span>حفظ
                            </button>
                            {{-- <button type="button" class="btn btn-warning" data-dismiss="modal">
                                 <span class='glyphicon glyphicon-remove'></span> إغلاق
                             </button>--}}
                            <a class="btn btn-primary" href="{{ route('admin.user.index') }}"> <span
                                        class='glyphicon glyphicon-remove'></span> رجوع</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
