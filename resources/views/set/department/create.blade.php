@extends("layouts.public")

@section("title")
    اضافة قسم جديدة
@endsection()

@section("css")

@endsection()

@section("content")
    <div class="content">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>تنبيه!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div set="dialog">
            <div class="modal-dialog" style="width: 80% ;font-size: larger">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row">
                            <div class="col-lg-12 margin-tb">
                                <div class="pull-left">
                                    <h1 class="text-center" style="font-family: 'Al-Jazeera'">
                                        {{"إضافة قسم جديد"}}
                                    </h1>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-primary" href="{{ route('set.department.index') }}"> رجوع</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        {!!Form::open(array('route' => 'set.department.store','method'=>'POST','class'=>'form-horizontal','set'=>'form')) !!}
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="title">اسم قسم</label>
                            <div class="col-sm-9">
                                {!! Form::text('name', null, array('placeholder' => 'اسم قسم','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="title">وصف قسم</label>
                            <div class="col-sm-9">
                                {!! Form::text('description', null, array('placeholder' => 'وصف قسم','class' => 'form-control')) !!}
                            </div>
                        </div>



                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success add">
                                <span class='glyphicon glyphicon-check'></span>حفظ
                            </button>

                            <a class="btn btn-primary" href="{{ route('set.department.store') }}"> <span
                                    class='glyphicon glyphicon-remove'></span> رجوع</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
