<head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta charset="utf-8" />
    <title>CMS</title>
    <meta name="description" content="Blank inner page examples">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">



    {{-- Left to right --}}
    {{-- <link href="{{asset('libraries/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('libraries/assets/demo/demo12/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />--}}



    {{-- Right to left --}}
    <link href="{{asset('libraries/assets/vendors/base/vendors.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('libraries/assets/demo/demo12/base/style.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />

{{--    profile  --}}
    <!--RTL version:<link href="../assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="{{asset('libraries/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />





    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" />







    <!-- <link rel="shortcut icon" href="~/assets/demo/demo12/media/img/logo/favicon.ico" /> -->

    <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
    <style>
        body {
            font-family: 'Cairo', sans-serif;
        }

        .pagination {
            float: left !important;
        }
    </style>

    @yield('css')
</head>
