<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<!-- هذا للروابط السي اس اس -->
@includeif('base_layout.header.meta_header')

@yield('style')


<!-- end::Head -->
<!-- begin::Body -->

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- BEGIN: Header -->
@includeif('base_layout.header.header')

    <!-- END: Header -->
    <!-- begin::Body -->
    <div style="padding-top:51px !important" class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <!-- BEGIN: Left Aside -->


    @includeif('base_layout.Navbars.navbar')
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <div class="m-content">
                @yield('content')

            </div>
        </div>
    </div>

    <!-- end:: Body -->
    <!-- begin::Footer -->
@includeif('base_layout.fotter.fotter')

    <!-- end::Footer -->
</div>


<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>
<div class="modal fade" id="PopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">


            <div class="modal-header">
                <h5 class="modal-title"></h5>

                <div data-dismiss="modal" class="m-demo-icon__preview">
                    <i style="color:white" class="la la-close"></i>
                </div>

            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="Confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Warning !</h5>
            </div>
            <div class="modal-body">
                <p>Are You Shore ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" data-action="delete-user">Yes</button>
            </div>
        </div>
    </div>
</div>


@includeif('base_layout.fotter.meta_fotter')



@yield('script')
</body>

<!-- end::Body -->

</html>
