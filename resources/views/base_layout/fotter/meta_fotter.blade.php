<script src="{{asset('libraries/nprogress-master/nprogress.js')}}"></script>

<script src="{{asset('libraries/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('libraries/assets/demo/demo12/base/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('libraries/assets/demo/demo12/base/datatables/basic.js')}}" type="text/javascript"></script>
<script src="{{asset('libraries/assets/demo/demo12/base/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('libraries/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script>
    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    $(".ajaxForm").ajaxForm({
        success: function(json) {
            if (json.status == 1) {

                var tname = $('.ajaxForm').attr("tname");
                var fname = $('.ajaxForm').attr("fname");

                if (tname != null) {
                    RefreshData(tname);
                }
                if (fname != null) {
                    eval(fname);
                }

                if (!$("#tblItems").hasClass("autohide")) {
                    $(".ajaxForm").resetForm();
                    $("#tblItems tbody tr").remove();
                    $("#tblItems").addClass("hidden").next().show();

                } else {
                    $(".select2").val('').change();
                }
            }
            console.log(json.close);
            if (json.msg != null && json.title != null)
                ShowMessage(json.msg, json.title);

            if (json.redirect != null)
                window.location = json.redirect;
            if (json.close == 1)
                $("#PopUp").modal("hide");
            $(".ajaxForm :submit").prop("disabled", false);
        },
        beforeSubmit: function() {
            $(".ajaxForm :submit").prop("disabled", true);
        }
    });


    $(document).on("click", ".PopUp", function() {
        if ($(this).attr("href") != '') {
            $("#PopUp").modal("show");
            $("#PopUp .modal-body").html('<h3 class="text-center text-danger"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></h3>');
            $("#PopUp .modal-body").load($(this).attr("href"), function() {
                setTimeout(function() {
                    $("#PopUp [autofocus]").focus().select();
                }, 500);
            });
            $("#PopUp .modal-title").text($(this).attr("title"));
            return false;
        }
    });


    $(document).on("click", ".Confirm", function() {
        $("#Confirm").modal("show");
        $("#Confirm .btn-danger").attr("href", $(this).attr("href"));
        $("#Confirm .btn-danger").attr("tname", $(this).attr("tname"));
        return false;
    });


    $("#Confirm .btn-danger").click(function() {
        var tname = $(this).attr("tname");
        var fname = $('.ajaxForm').attr("fname");
        var url = $(this).attr("href");
        var request = $.ajax({
            type: 'delete',
            url: url,
            success: function(json) {
                if (json.status == 1) {
                    if (tname != null) {
                        RefreshData(tname);
                    }
                    if (fname != null) {
                        eval(fname);
                    }
                }
                ShowMessage(json.msg, json.title);
            }
        });
        request.fail(function(response, exception) {
            // http.fail(JSON.parse(response.responseText), true);
            alert(response.responseText);
        });
        $("#Confirm").modal("hide");
        return false;
    });

    function RefreshData(tn) {
        var xtable = $("#" + tn).dataTable();
        var info = $("#" + tn).DataTable().page.info();
        xtable.fnDraw();
        xtable.fnPageChange(info.page);
    }


    $(document).ajaxStart(function() {
        $(".wait").text("Wait...");
        NProgress.start();
    });
    $(document).ajaxStop(function() {
        $(".wait").text("");
        NProgress.done();
    });
    $(document).ajaxError(function() {
        $(".wait").text("");
        NProgress.done();
        $(".ajaxForm :submit").prop("disabled", false);
    });


    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    function ShowMessage(msg, title = "defualt") {
        var cls = "info";
        if (msg.indexOf("s:") == 0) {
            cls = "success";
            msg = msg.substring(2);
        }
        if (msg.indexOf("w:") == 0) {
            cls = "warning";
            msg = msg.substring(2);
        }
        if (msg.indexOf("e:") == 0) {
            cls = "error";
            msg = msg.substring(2);
        }
        if (msg.indexOf("i:") == 0) {
            cls = "info";
            msg = msg.substring(2);
        }
        toastr[cls](msg, title);
    }

    function PageLoadActions() {
        $(".date").datepicker({
            format: "dd/mm/yyyy",
            endDate: "+0d",
            autoclose: true
        });

        $(".input-daterange").datepicker({
            format: "dd/mm/yyyy",
            endDate: "+0d",
            autoclose: true
        });

        $(".ajaxForm").ajaxForm({
            success: function(json) {
                if (json.status == 1) {

                    var tname = $('.ajaxForm').attr("tname");
                    var fname = $('.ajaxForm').attr("fname");

                    if (tname != null) {
                        RefreshData(tname);
                    }
                    if (fname != null) {
                        eval(fname);
                    }

                    if (!$("#tblItems").hasClass("autohide")) {
                        $(".ajaxForm").resetForm();
                        $("#tblItems tbody tr").remove();
                        $("#tblItems").addClass("hidden").next().show();

                    } else {
                        $(".select2").val('').change();
                    }
                }
                console.log(json.close);
                if (json.msg != null)
                    ShowMessage(json.msg, json.title);

                if (json.redirect != null)
                    window.location = json.redirect;
                if (json.close == 1)
                    $("#PopUp").modal("hide");
                $(".ajaxForm :submit").prop("disabled", false);
            },
            beforeSubmit: function() {
                $(".ajaxForm :submit").prop("disabled", true);
            },
            // if we get error we'll do these things =>
            error: function(err) {
                // first convert the error text to json object
                var listErr = JSON.parse(err.responseText);
                // then check if we get the same status code that we have sent in previous
                if (err.status == 422) {
                    // get all span tags
                    var fields = document.querySelectorAll('.error');
                    // then loop throw them
                    for (const element of fields) {
                        // get each span name
                        var name = element.getAttribute("name")
                        // then trim the first charcater like this=>
                        var name = name.substring(1);
                        console.log(listErr);
                        // now we want to check if this input have an error or not
                        if(listErr.errors[name]){
// if so we will do this
                            element.textContent = listErr.errors[name][0]
                        }else{
                            // if not we will asssign an empty string like this=>
                            element.textContent = "";
                        }
                    }
                    // in the end will show an error msg like this =>
                    ShowMessage(JSON.parse(err.responseText).msg, 'خطأ');
                }
            }
        });
    }







{{--    function RefreshGallery(data)--}}
{{--    {--}}


{{--// هنا هنفحص هل هذا اول عنصر اضيفه علشان امسح الكلام المكتوب مسبقا--}}
{{--        if(data.count - 1 === 0 )--}}
{{--        {--}}
{{--            document.getElementById('GallariesSection').textContent='';--}}

{{--        }--}}

{{--        if(data.count > 0 )--}}
{{--        {--}}
{{--            $("#GallariesSection").append('<div class="card">'+'<a href="{{route('albums.show')}}/'+data[0].id+'"'+'>'+'<img class="card-img-top img-fluid" '+--}}
{{--                'src="{{asset("/storage/album_covers")}}'+ '/' + data[0].cover_image+'" alt="'+--}}
{{--                data[0].name +'"></a><div class="card-body"><h4 class="card-title">'+data[0].name+'</h4><p class="card-text">'+ data[0].description+'</p><p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p></div></div>');--}}

{{--            if (data.msg != null && data.title != null)--}}
{{--            {--}}
{{--                ShowMessage(data.msg, data.title);--}}

{{--            }--}}

{{--            if (data.redirect != null)--}}
{{--                window.location = data.redirect;--}}
{{--            if (data.close == 1)--}}
{{--                $("#PopUp").modal("hide");--}}
{{--            $(".GalleryForm :submit").prop("disabled", false);--}}
{{--        }else{--}}
{{--            document.getElementById('GallariesSection').textContent='No Albums To Display';--}}

{{--        }--}}



{{--    }--}}

    function processJson(data){
        console.log(data)
        RefreshGallery(data);
    }


    function updateGallery() {

        $(".GalleryForm").ajaxForm({
            success: processJson ,
            beforeSubmit: function() {
                $(".GalleryForm :submit").prop("disabled", true);
            },
            // if we get error we'll do these things =>
            error: function(err) {
                // first convert the error text to json object
                var listErr = JSON.parse(err.responseText);
                // then check if we get the same status code that we have sent in previous
                if (err.status == 422) {
                    // get all span tags
                    var fields = document.querySelectorAll('.error');
                    // then loop throw them
                    for (const element of fields) {
                        // get each span name
                        var name = element.getAttribute("name")
                        // then trim the first charcater like this=>
                        var name = name.substring(1);
                        console.log(listErr);
                        // now we want to check if this input have an error or not
                        if(listErr.errors[name]){
// if so we will do this
                            element.textContent = listErr.errors[name][0]
                        }else{
                            // if not we will asssign an empty string like this=>
                            element.textContent = "";
                        }
                    }
                    $(".GalleryForm :submit").prop("disabled", false);

                    // in the end will show an error msg like this =>
                    ShowMessage(JSON.parse(err.responseText).msg, 'خطأ');
                }
            }
        });

    }






</script>
