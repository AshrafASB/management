<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
            class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
         m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
        <ul class="m-menu__nav ">

            <li class="m-menu__item " aria-haspopup="true"><a href="{{route('department.home')}}" class="m-menu__link "><span
                            class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span
                            class="m-menu__link-text">الرئيسية</span></a></li>


            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle">
                    <span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-layers"></i><span
                            class="m-menu__link-text">ادارة المستخدمين</span><i
                            class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">

                        <li class="m-menu__item " aria-haspopup="true"><a href="{{route('department.user.index')}}"
                                                                          class="m-menu__link "><i
                                        class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span
                                        class="m-menu__link-text">المستخدمين</span></a></li>

                    </ul>
                </div>
            </li>

            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="{{route('department.task.index')}}" class="m-menu__link m-menu__toggle">
                    <span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-layers"></i><span
                            class="m-menu__link-text">ادارة المهام</span><i
                            class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
            </li>

            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="{{route('department.ach.index')}}" class="m-menu__link m-menu__toggle">
                    <span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-layers"></i><span
                            class="m-menu__link-text">ادارة الإنجازات</span><i
                            class="m-menu__ver-arrow la la-angle-right"></i>
                </a>

            </li>

        </ul>
    </div>

    <!-- END: Aside Menu -->
</div>
