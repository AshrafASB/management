<form method="post" class="ajaxForm" tname="AchTable" action="{{route('department.ach.store')}}"
      enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">اسم الإنجاز</label>
                <input type="text" name="name" class="form-control" required/>
                <span name="nname" class="text-danger error"></span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">وصف الإنجاز</label>
                <input type="text" name="description" class="form-control" required/>
                <span name="edescription" class="text-danger error"></span>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="task">اسم المهمة</label>
                <select name="task" id="task" class="form-control">
                    <option value="-1" selected>اختر مهمة</option>
                    @foreach($tasks as $task)

                    <option value="{{$task->id}}" >{{$task->name}}</option>
                    @endforeach
                </select>

            </div>
        </div>
    </div>





    <div class="form-group">
        <input type="submit" value="اضافة" class="btn btn-primary"/>
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">الغاء الامر</button>
    </div>
</form>


<form asp-action="Create" method="post" class="ajaxForm" tname="AchTable">
    <div asp-validation-summary="ModelOnly" class="text-danger"></div>

</form>


<script>
    PageLoadActions();
</script>
