@extends('base_layout.master_layout')
@section('title','test')
@section('style')
    <style>
        td {
            padding: 4px 1px 0px 1px !important;
            font-size: 11px !important
        }

        td a {
            margin-left: 5px !important;
        }

        .m-portlet__body {
            padding-top: 0px !important;
        }
    </style>
@endsection
@section('content')


    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <span style="color : lightslategray">فهرس المستخدمين في القسم</span><small style="color : gray">عرض وتعديل بيانات المستخدمين </small>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{route('department.user.create')}}" title="اضافة مستخدم  جديد " class="PopUp btn-sm btn btn-outline-primary m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
                        <span>
                            <i class="fa fa-plus-square"></i>
                            <span>Add</span>
                        </span>
                        </a>
                    </li>
                    <li class="m-portlet__nav-item">
                        <a href="#" title="تصدير الى اكسل" class="btn btn-sm btn-outline-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
                        <span>
                            <i class="fa fa-file-excel"></i>
                            <span>Excel</span>
                        </span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
        <div class="m-portlet__body">

            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-10 m--margin-bottom-0">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-6">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" name="SearchKey" class="form-control m-input m-input--air" placeholder="اسم المستخد.. ">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <a class="SearchBtn btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--custom m-btn--outline-1x m-btn--pill m-btn--air">
                                    <i class="fa fa-search"></i>
                                </a>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <table class="table m-table m-table--head-separator-primary" id="UsersTable">

                <!-- <table class="table table-bordered data-table"> -->
                <thead>
                <tr>
                    <th>الاسم	</th>
                    <th>الايميل	</th>
                    <th>الدائرة	</th>
                    <th>القسم	</th>
                    <th>الصلاحية	</th>
                    <th>تاريخ	</th>
                    <th width="100px">العمليات</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>

    </div>


@endsection
@section('script')


    <script type="text/javascript">
        $(function() {

            var table = $('#UsersTable').DataTable({
                language: {
                    aria: {
                        sortAscending: ": فعال لترتيب العمود تصاعديا",
                        sortDescending: ": فعال لترتيب العمود تنازليا"
                    },
                    emptyTable: "لا يوجد بيانات لعرضها",
                    info: "عرض _START_ الى _END_ من _TOTAL_ صف",
                    infoEmpty: "لا يوجد نتائج لعرضها",
                    infoFiltered: "(filtered1 من _MAX_ اجمالي صفوف)",
                    lengthMenu: "_MENU_",
                    search: "بحث",
                    zeroRecords: "لا يوجد نتائج لعرضها",
                    paginate: {
                        sFirst: "الاول",
                        sLast: "الاخير",
                        sNext: "التالي",
                        sPrevious: "السابق"
                    }
                },
                "bLengthChange": false,
                "deferRender": true,
                processing: true,
                serverSide: true,
                "searching": false,

                ajax: "{{ route('department.getUsers') }}",

                columns: [{
                    data: 'name',
                    name: 'name'
                },

                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'SetName',
                        name: 'SetName'
                    },
                    {
                        data: 'DepName',
                        name: 'DepName'
                    },

                    {
                        data: 'RoleName',
                        name: 'RoleName'
                    },




                    {
                        data: 'created_at',
                        name: 'created_at'
                    },





                    {
                        data: 'Actions',
                        name: 'Actions',
                        orderable: false,
                        searchable: false
                    },
                ],

            });

        });

    </script>
@endsection
