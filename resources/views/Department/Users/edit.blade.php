<form method="post" class="ajaxForm" tname="UsersTable" action="{{route('department.user.update',$user->id)}}" >

    @csrf
    @method('PUT')

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label  class="control-label">اسم المستخدم</label>
                <input name="name" class="form-control" required value="{{$user->name}}" />
                <span name="nname" class="text-danger error"></span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label  class="control-label">البريد الالكتروني</label>
                <input type="email" name="email" class="form-control" disabled value="{{$user->email}}"/>
                <span name="eemail" class="text-danger error"></span>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-6">

            <div class="form-group">
                <label  class="control-label">كلمة المرور</label>
                <input name="password" class="form-control"  required type="password" disabled/>
                <span name="ppassword" class="text-danger error"></span>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label  class="control-label">تاكيد كلمة المرور</label>
                <input name="password_confirmation" class="form-control"  required type="password" disabled/>
                <span name="password_confirmation" class="text-danger error"></span>
            </div>
        </div>


    </div>







    <div class="form-group">
        <input type="submit" value="تعديل" class="btn btn-primary" />
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">الغاء الامر</button>
    </div>
</form>























<form  asp-action="Create" method="post" class="ajaxForm" tname="AdsTable">
    <div asp-validation-summary="ModelOnly" class="text-danger"></div>

</form>


<script>
    PageLoadActions();
</script>
