<form method="post" class="ajaxForm" tname="TaskTable" action="{{route('department.task.update',$task->id)}}"
      enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">اسم المهمة</label>
                <input type="text" name="name" class="form-control" required value="{{$task->name}}"/>
                <span name="nname" class="text-danger error"></span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">وصف المهمة</label>
                <input type="text" name="description" class="form-control" required value="{{$task->description}}"/>
                <span name="edescription" class="text-danger error"></span>
            </div>
        </div>

    </div>


    <div class="form-group">
        <input type="submit" value="تعديل" class="btn btn-primary"/>
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">الغاء الامر</button>
    </div>
</form>




<script>
    PageLoadActions();
</script>
