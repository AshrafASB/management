@extends("layouts.public")

@section("title")
    المهام
@endsection()

@section("css")

@endsection()

@section("content")

    <div class="content">
        <div class="tab-content">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h1 class="text-center" style="font-family: 'Al-Jazeera'">
                        {{"إدارة المهام"}}
                    </h1>
                    <div class="heading-elements">
                        <a class="btn btn-success" href="{{ route('admin.task.create') }}">{{" إضافة مهمة جديد"}}</a>
                    </div>
                </div>

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="row" style="font-size: initial">
                    <div class="col-md-12">
                        <table class="table datatable-basic">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>عنوان المهمة</th>
                                <th>وصف المهام</th>
                                <th>الدائرة</th>
                                <th>القسم</th>
                                <th>نسبة الانجاز</th>
                                <th>التاريخ</th>
                                <th>الحدث</th>
                            </tr>
                            </thead>
                            @foreach ($data as $key => $item)
                                <tr>
                                    <td>{{$item->id }}</td>
                                    <td>{{$item->name }}</td>
                                    <td>{{$item->description }}</td>
                                    <td>{{$item->department->set->name }}</td>
                                    <td>{{$item->department->name }}</td>
                                    <td>{{$item->progress}}</td>
                                    <td>{{ date('d-m-Y', strtotime($item->created_at)) }} </td>
                                    <td style="width: 22%">
                                        <a class="edit-modal btn btn-info"
                                           href="{{ route('admin.set.edit',$item->id) }}">
                                            <i class="glyphicon glyphicon-edit"></i> تعديل
                                        </a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['admin.task.destroy', $item->id],'style'=>'display:inline']) !!}
                                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i> حذف', ['type' => 'submit','class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection()

@section("js")
    <!-- Theme JS files -->
    <script src="{{asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script src="{{asset('global_assets/js/demo_pages/datatables_basic.js') }}"></script>
    <!-- /theme JS files -->
@endsection()
