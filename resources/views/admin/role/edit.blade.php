@extends("layouts.public")

@section("title")
    تعديل مهمة
@endsection()

@section("css")

@endsection()

@section("content")
    <div class="content">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>تنبيه!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div role="dialog">
            <div class="modal-dialog" style="width: 80% ;font-size: larger">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row">
                            <div class="col-lg-12 margin-tb">
                                <div class="pull-left">
                                    <h1 class="text-center" style="font-family: 'Al-Jazeera'">
                                        {{"تعديل مهمة"}}
                                    </h1>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-primary" href="{{ route('admin.role.index') }}"> رجوع</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        {!!Form::open(array('route' =>['admin.role.update',$role->id] ,'method'=>'POST','class'=>'form-horizontal','role'=>'form')) !!}
                        @method('PUT')
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="title">اسم الصلاحية</label>
                            <div class="col-sm-9">
                                {!! Form::text('name', value($role->name), array('placeholder' => 'اسم الصلاحية','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="title">وصف الصلاحية</label>
                            <div class="col-sm-9">
                                {!! Form::text('description', value($role->description), array('placeholder' => 'وصف الصلاحية','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success add">
                                <span class='glyphicon glyphicon-check'></span>حفظ
                            </button>

                            <a class="btn btn-primary" href="{{ route('admin.role.index') }}"> <span
                                    class='glyphicon glyphicon-remove'></span> رجوع</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
