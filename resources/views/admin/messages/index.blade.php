@extends("layouts.public")

@section("title")
    الرسائل
@endsection()

@section("css")

    {{--
        <!-- Global stylesheets -->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/layout.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/components.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/colors.min.css')}}" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->--}}
@endsection()

@section("content")

    <div class="page-content">
        <!-- Secondary sidebar -->
        <div class="sidebar sidebar-secondary sidebar-default">
            <div class="sidebar-content">

                <!-- Actions -->
                <div class="sidebar-category">
                    <div class="category-title">
                        <span>العمليات</span>
                        <ul class="icons-list">
                            <li><a href="#" data-action="collapse"></a></li>
                        </ul>
                    </div>

                    <div class="category-content">
                        <a href="{{route('admin.messages.create')}}" class="btn bg-indigo-400 btn-block legitRipple">انشاء بريد</a>
                    </div>
                </div>
                <!-- /actions -->


                <!-- Sub navigation -->
                <div class="sidebar-category">
                    <div class="category-title">
                        <span>قائمة</span>
                        <ul class="icons-list">
                            <li><a href="#" data-action="collapse"></a></li>
                        </ul>
                    </div>

                    <div class="category-content no-padding">
                        <ul class="navigation navigation-alt navigation-accordion no-padding-bottom">
                            <li class="navigation-header">المجلدات</li>
                            <li class="active"><a href="#" class="legitRipple"><i class="icon-drawer-in"></i> الوارد
                                    <span class="badge badge-success">32</span></a></li>
                            <li><a href="#" class="legitRipple"><i class="icon-drawer-out"></i> الصادر</a></li>
                            <li><a href="#" class="legitRipple"><i class="icon-stars"></i> المفضلة</a></li>
                            <li class="navigation-divider"></li>
                            <li><a href="#" class="legitRipple"><i class="icon-spam"></i> المزعجة <span
                                        class="badge badge-danger">99+</span></a></li>
                            <li><a href="#" class="legitRipple"><i class="icon-bin"></i> المحذوفة</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /sub navigation -->


                <!-- Online users -->
                <div class="sidebar-category">
                    <div class="category-title">
                        <span>المستخدمين</span>
                        <ul class="icons-list">
                            <li><a href="#" data-action="collapse"></a></li>
                        </ul>
                    </div>

                    <div class="category-content no-padding">
                        <ul class="media-list media-list-linked">
                            <li class="media">
                                <a href="#" class="media-link">
                                    <div class="media-left"><img
                                            src="../../../../global_assets/images/placeholders/placeholder.jpg"
                                            class="img-circle img-md" alt=""></div>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">James Alexander</span>
                                        <span class="text-size-small text-muted display-block">UI/UX expert</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <span class="status-mark bg-success"></span>
                                    </div>
                                </a>
                            </li>

                            <li class="media">
                                <a href="#" class="media-link">
                                    <div class="media-left"><img
                                            src="../../../../global_assets/images/placeholders/placeholder.jpg"
                                            class="img-circle img-md" alt=""></div>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">Jeremy Victorino</span>
                                        <span class="text-size-small text-muted display-block">Senior designer</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <span class="status-mark bg-danger"></span>
                                    </div>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <!-- /online users -->


                <!-- Latest messages -->
                <div class="sidebar-category">
                    <div class="category-title">
                        <span>الرسائل الاخيرة</span>
                        <ul class="icons-list">
                            <li><a href="#" data-action="collapse"></a></li>
                        </ul>
                    </div>

                    <div class="category-content no-padding">
                        <ul class="media-list media-list-linked">
                            <li class="media">
                                <a href="#" class="media-link">
                                    <div class="media-left"><img
                                            src="../../../../global_assets/images/placeholders/placeholder.jpg"
                                            class="img-circle img-md" alt=""></div>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">Will Samuel</span>
                                        <span class="text-muted">And he looked over at the alarm clock, ticking..</span>
                                    </div>
                                </a>
                            </li>

                            <li class="media">
                                <a href="#" class="media-link">
                                    <div class="media-left"><img
                                            src="../../../../global_assets/images/placeholders/placeholder.jpg"
                                            class="img-circle img-md" alt=""></div>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">Margo Baker</span>
                                        <span class="text-muted">However hard he threw himself onto..</span>
                                    </div>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <!-- /latest messages -->

            </div>
        </div>
        <!-- /secondary sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">صندوق الرسائل</span> -
                            الكل</h4>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

                    <div class="heading-elements">
                        <form class="heading-form" action="#">
                            <div class="form-group">
                                <div class="has-feedback">
                                    <input type="search" class="form-control" placeholder="ابحث عن رسائل">
                                    <div class="form-control-feedback">
                                        <i class="icon-search4 text-size-small text-muted"></i>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i
                            class="icon-menu-open"></i></a>
                    <ul class="breadcrumb">
                        <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> الصفحة الرئيسية</a></li>
                        <li><a href="{{route('admin.messages.index')}}">صندوق الرسائل</a></li>
                        <li class="active">الكل</li>
                    </ul>


                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Multiple lines -->
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h6 class="panel-title">بريدي</h6>

                        <div class="heading-elements not-collapsible">
                            <span class="label bg-blue heading-text">المجموع:</span>
                        </div>
                    </div>

                    <div class="panel-toolbar panel-toolbar-inbox">
                        <div class="navbar navbar-default">
                            <ul class="nav navbar-nav visible-xs-block no-border">
                                <li>
                                    <a class="text-center collapsed legitRipple" data-toggle="collapse"
                                       data-target="#inbox-toolbar-toggle-multiple">
                                        <i class="icon-circle-down2"></i>
                                    </a>
                                </li>
                            </ul>

                            <div class="navbar-collapse collapse" id="inbox-toolbar-toggle-multiple">
                                <div class="btn-group navbar-btn">
                                    <button type="button" class="btn btn-default btn-icon btn-checkbox-all legitRipple">
                                        <div class="checker"><span><input type="checkbox" class="styled"></span></div>
                                    </button>

                                    <button type="button" class="btn btn-default btn-icon dropdown-toggle legitRipple"
                                            data-toggle="dropdown">
                                        <span class="caret"></span>
                                    </button>

                                    <ul class="dropdown-menu">
                                        <li><a href="#">Select all</a></li>
                                        <li><a href="#">Select read</a></li>
                                        <li><a href="#">Select unread</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Clear selection</a></li>
                                    </ul>
                                </div>

                                <div class="btn-group navbar-btn">
                                    <button type="button" class="btn btn-default legitRipple"><i
                                            class="icon-pencil7"></i> <span
                                            class="hidden-xs position-right">انشاء بريد</span></button>
                                    <button type="button" class="btn btn-default legitRipple"><i class="icon-bin"></i>
                                        <span class="hidden-xs position-right">حذف</span></button>
                                    <button type="button" class="btn btn-default legitRipple"><i class="icon-spam"></i>
                                        <span class="hidden-xs position-right">رسائل دعائية</span></button>
                                </div>

                                <div class="navbar-right">
                                    <p class="navbar-text"><span class="text-semibold">1-50</span> of <span
                                            class="text-semibold">528</span></p>

                                    <div class="btn-group navbar-left navbar-btn">
                                        <button type="button" class="btn btn-default btn-icon disabled"><i
                                                class="icon-arrow-right13"></i></button>
                                        <button type="button" class="btn btn-default btn-icon legitRipple"><i
                                                class="icon-arrow-left12"></i></button>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-inbox">
                            <tbody data-link="row" class="rowlink">
                         @foreach($messages as $message)
                             <tr class="{{($message->isRead)? 'read' : 'unread'}}">
                                 <td class="table-inbox-checkbox rowlink-skip">
                                     <div class="checker"><span><input type="checkbox" class="styled"></span></div>
                                 </td>
                        {{--              Stars                   --}}
                                 <td class="table-inbox-star rowlink-skip">
                                     <a href="mail_read.html">
                                         <i class="{{($message->isStarred)? 'icon-star-full2 text-warning-300' : 'icon-star-empty3 text-muted'}}"></i>

                                     </a>
                                 </td>
                                 {{--              Gravatar                   --}}

                                 <td class="table-inbox-image">
                                     <img src="{{Gravatar::src(auth()->user()->email)}}"
                                          class="img-circle img-xs" alt="">
                                 </td>
                                 {{--              name                   --}}

                                 <td class="table-inbox-name">
                                     <a href="{{route('admin.messages.show',$message->id)}}">
                                         <div class="letter-icon-title text-default">{{$message->send->name}}</div>
                                     </a>
                                 </td>
                                 {{--              Message                   --}}

                                 <td class="table-inbox-message">
                                     <div class="table-inbox-subject">{{$message->title}}</div>
                                     <span class="table-inbox-preview">{{$message->description}}</span>
                                 </td>

                                 <td class="table-inbox-time">
                                    {{$message->created_at->diffForHumans()}}
                                 </td>
                             </tr>


                             @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /multiple lines -->


                <!-- Footer -->
                <div class="footer text-muted">
                    © 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov"
                                                                        target="_blank">Eugene Kopyov</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>


@endsection()

@section("js")
    {{--    <!-- Theme JS files -->
        <script src="{{asset('global_assets/js/plugins/extensions/rowlink.js') }}"></script>
        <script src="{{asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

        <script src="{{asset('assets/js/app.js') }}"></script>
        <script src="{{asset('global_assets/js/demo_pages/mail_list.js') }}"></script>
        <!-- /theme JS files -->--}}
@endsection()
