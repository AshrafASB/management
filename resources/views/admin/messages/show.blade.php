@extends("layouts.public")

@section("title")
    انشاء
@endsection()

@section("content")

    <div class="page-content">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>تنبيه!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif

    <!-- Secondary sidebar -->
        <div class="sidebar sidebar-secondary sidebar-default">
            <div class="sidebar-content">

                <!-- Actions -->
                <div class="sidebar-category">
                    <div class="category-title">
                        <span>العمليات</span>
                        <ul class="icons-list">
                            <li><a href="#" data-action="collapse"></a></li>
                        </ul>
                    </div>

                    <div class="category-content">
                        <a href="{{route('admin.messages.create')}}" class="btn bg-indigo-400 btn-block legitRipple">انشاء
                            بريد</a>
                    </div>
                </div>
                <!-- /actions -->


                <!-- Sub navigation -->
                <div class="sidebar-category">
                    <div class="category-title">
                        <span>قائمة</span>
                        <ul class="icons-list">
                            <li><a href="#" data-action="collapse"></a></li>
                        </ul>
                    </div>

                    <div class="category-content no-padding">
                        <ul class="navigation navigation-alt navigation-accordion no-padding-bottom">
                            <li class="navigation-header">المجلدات</li>
                            <li class="active"><a href="#" class="legitRipple"><i class="icon-drawer-in"></i> الوارد
                                    <span class="badge badge-success">32</span></a></li>
                            <li><a href="#" class="legitRipple"><i class="icon-drawer-out"></i> الصادر</a></li>
                            <li><a href="#" class="legitRipple"><i class="icon-stars"></i> المفضلة</a></li>
                            <li class="navigation-divider"></li>
                            <li><a href="#" class="legitRipple"><i class="icon-spam"></i> المزعجة <span
                                        class="badge badge-danger">99+</span></a></li>
                            <li><a href="#" class="legitRipple"><i class="icon-bin"></i> المحذوفة</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /sub navigation -->


                <!-- Online users -->
                <div class="sidebar-category">
                    <div class="category-title">
                        <span>المستخدمين</span>
                        <ul class="icons-list">
                            <li><a href="#" data-action="collapse"></a></li>
                        </ul>
                    </div>

                    <div class="category-content no-padding">
                        <ul class="media-list media-list-linked">
                            <li class="media">
                                <a href="#" class="media-link">
                                    <div class="media-left"><img
                                            src="global_assets/images/placeholders/placeholder.jpg"
                                            class="img-circle img-md" alt=""></div>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">James Alexander</span>
                                        <span class="text-size-small text-muted display-block">UI/UX expert</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <span class="status-mark bg-success"></span>
                                    </div>
                                </a>
                            </li>

                            <li class="media">
                                <a href="#" class="media-link">
                                    <div class="media-left"><img
                                            src="global_assets/images/placeholders/placeholder.jpg"
                                            class="img-circle img-md" alt=""></div>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">Jeremy Victorino</span>
                                        <span class="text-size-small text-muted display-block">Senior designer</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <span class="status-mark bg-danger"></span>
                                    </div>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <!-- /online users -->


                <!-- Latest messages -->
                <div class="sidebar-category">
                    <div class="category-title">
                        <span>الرسائل الاخيرة</span>
                        <ul class="icons-list">
                            <li><a href="#" data-action="collapse"></a></li>
                        </ul>
                    </div>

                    <div class="category-content no-padding">
                        <ul class="media-list media-list-linked">
                            <li class="media">
                                <a href="#" class="media-link">
                                    <div class="media-left"><img
                                            src="global_assets/images/placeholders/placeholder.jpg"
                                            class="img-circle img-md" alt=""></div>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">Will Samuel</span>
                                        <span class="text-muted">And he looked over at the alarm clock, ticking..</span>
                                    </div>
                                </a>
                            </li>

                            <li class="media">
                                <a href="#" class="media-link">
                                    <div class="media-left"><img
                                            src="global_assets/images/placeholders/placeholder.jpg"
                                            class="img-circle img-md" alt=""></div>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">Margo Baker</span>
                                        <span class="text-muted">However hard he threw himself onto..</span>
                                    </div>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <!-- /latest messages -->

            </div>
        </div>
        <!-- /secondary sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-right6 position-left"></i> <span
                                class="text-semibold">صندوق الرسائل</span> -
                            الكل</h4>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

                    <div class="heading-elements">
                        <form class="heading-form" action="#">
                            <div class="form-group">
                                <div class="has-feedback">
                                    <input type="search" class="form-control" placeholder="ابحث عن رسائل">
                                    <div class="form-control-feedback">
                                        <i class="icon-search4 text-size-small text-muted"></i>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i
                            class="icon-menu-open"></i></a>
                    <ul class="breadcrumb">
                        <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> الصفحة الرئيسية</a>
                        </li>
                        <li><a href="{{route('admin.messages.index')}}">صندوق الرسائل</a></li>
                        <li class="active">عرض رسالة</li>
                    </ul>


                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Multiple lines -->
                <div class="panel panel-white">
                    <div class="content-detached">

                        <form method="post" action="{{route('admin.messages.store')}}">
                        @csrf
                        <!-- Single mail -->
                            <div class="panel panel-white">

                                <div class="content-detached">

                                    <!-- Single mail -->
                                    <div class="panel panel-white">

                                        <!-- Mail toolbar -->
                                        <div class="panel-toolbar panel-toolbar-inbox">
                                            <div class="navbar navbar-default">
                                                <ul class="nav navbar-nav visible-xs-block no-border">
                                                    <li>
                                                        <a class="text-center collapsed" data-toggle="collapse" data-target="#inbox-toolbar-toggle-single">
                                                            <i class="icon-circle-down2"></i>
                                                        </a>
                                                    </li>
                                                </ul>

                                                <div class="navbar-collapse collapse" id="inbox-toolbar-toggle-single">
                                                    <div class="btn-group navbar-btn">

                                                        <a href="#" class="btn btn-default"><i class="icon-bin"></i> <span class="hidden-xs position-right">حذف</span></a>

                                                    </div>

                                                    <div class="pull-right-lg">
                                                        <p class="navbar-text">{{$msg->created_at->diffForHumans()}}</p>
                                                        <div class="btn-group navbar-btn">
                                                            <a href="#" class="btn btn-default"><i class="icon-printer"></i> <span class="hidden-xs position-right">طباعة</span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /mail toolbar -->


                                        <!-- Mail details -->
                                        <div class="media stack-media-on-mobile mail-details-read">
                                            <a href="#" class="media-left">
										<span class="btn bg-teal-400 btn-rounded btn-icon btn-xlg">
											<span class="letter-icon">A</span>
										</span>
                                            </a>

                                            <div class="media-body">
                                                <h6 class="media-heading">{{$msg->title}}</h6>
                                                <div class="letter-icon-title text-semibold">{{$msg->res->name}} <a href="#">{{$msg->res->email}}</a></div>
                                            </div>


                                        </div>
                                        <!-- /mail details -->


                                        <!-- Mail container -->
                                        <div class="mail-container-read">


                                                  <textarea class="summernote"  name="description" style="display: none;">
                                                      {!! $msg->description !!}
                                                  </textarea>
                                        </div>
                                        <!-- /mail container -->


                                        <!-- Attachments -->
                                        <div class="mail-attachments-container">
                                            <h6 class="mail-attachments-heading">2 Attachments</h6>

                                            <ul class="mail-attachments">
                                                <li>
											<span class="mail-attachments-preview">
												<i class="icon-file-pdf icon-2x"></i>
											</span>

                                                    <div class="mail-attachments-content">
                                                        <span class="text-semibold">new_december_offers.pdf</span>

                                                        <ul class="list-inline list-inline-condensed no-margin">
                                                            <li class="text-muted">174 KB</li>
                                                            <li><a href="#">View</a></li>
                                                            <li><a href="#">Download</a></li>
                                                        </ul>
                                                    </div>
                                                </li>

                                                <li>
											<span class="mail-attachments-preview">
												<i class="icon-file-pdf icon-2x"></i>
											</span>

                                                    <div class="mail-attachments-content">
                                                        <span class="text-semibold">assignment_letter.pdf</span>

                                                        <ul class="list-inline list-inline-condensed no-margin">
                                                            <li class="text-muted">736 KB</li>
                                                            <li><a href="#">View</a></li>
                                                            <li><a href="#">Download</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /attachments -->

                                    </div>
                                    <!-- /single mail -->

                                </div>

                            </div>
                            <!-- /single mail -->

                        </form>

                    </div>
                </div>
                <!-- /multiple lines -->


                <!-- Footer -->
                <div class="footer text-muted">
                    © 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov"
                                                                        target="_blank">Eugene Kopyov</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>


@endsection()

@section("js")
    <!-- Theme JS files -->
    <script src="{{asset('global_assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
    <script src="{{asset('global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

    <script src="{{asset('assets/js/app.js')}}"></script>
    <script src="{{asset('global_assets/js/demo_pages/mail_list_write.js')}}"></script>
    <!-- /theme JS files -->
@endsection()


