@extends("layouts.public")

@section("title")
    الصفحة الرئيسية
@endsection

@section("css")

@endsection()

@section("content")
    <!-- Content area -->
    <div class="content pt-0">
        <!-- Info alert -->
        <div class="alert alert-info bg-white alert-styled-left alert-arrow-left alert-dismissible"
             style="height: 160px;text-align: center;">
            <h1 class="alert-heading font-weight-semibold mb-1">ملخص الانجازات</h1>
            <div class="col-md-4">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                    <h4 class="widget-thumb-heading">إنجازات اليوم </h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-green icon-bulb"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">عدد</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="">0</span>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
            <div class="col-md-4">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                    <h4 class="widget-thumb-heading">الانجازات الشهرية</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-red icon-layers"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">عدد</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="">0</span>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
            <div class="col-md-4">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                    <h4 class="widget-thumb-heading">إجمالي الإنجازات </h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">عدد</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="">0</span>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
        </div>
        <!-- /info alert -->

        @forelse($items as $key=>$item)
            <div class="alert alert-info bg-white alert-styled-left alert-arrow-left alert-dismissible"
                 style="height: 160px;text-align: center;">
                <h1 class="alert-heading font-weight-semibold mb-1">{{$item->name}}</h1>
                <div class="col-md-4">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">إنجازات اليوم </h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-green icon-bulb"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle">عدد</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="">0</span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
                <div class="col-md-4">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">الانجازات الشهرية</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-red icon-layers"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle">عدد</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="">0</span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
                <div class="col-md-4">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">إجمالي الإنجازات </h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle">عدد</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="">0</span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
            </div>
            <!-- /info alert -->

        @empty
        @endforelse

    </div>
    <!-- /content area -->
    <!-- END PAGE HEADER-->
@endsection()

@section("js")
@endsection()
