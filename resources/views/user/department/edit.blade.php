@extends("Layouts._layout")

@section("title")
    تعديل قسم
@endsection()

@section("css")

@endsection()

@section("breadcramb")
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="{{route('home')}}">الرئيسية</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="{{route('admin.department.index')}}">الأقسام</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <span> تعديل قسم </span>
            </li>
        </ul>
    </div>
@endsection()

@section("content")
    <div class="row">
        <div class="col-md-8">
            <form action="{{route('admin.department.update',1)}}}" method="post" class="form-horizontal">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="fullname" class="col-sm-2 control-label">اسم القسم</label>
                    <div class="col-sm-10">
                        <input required type="text" autofocus class="form-control" value="{{old('name')}}" name="name" id="name" placeholder="اسم القسم">
                    </div>
                </div>
                <div class="form-group" id="sp-0">
                    <div class="col-md-2">
                        <label for="fullname" class="control-label"> إضافة مهمة </label>
                    </div>
                    <div class="col-md-10" required>
                        <div class="col-md-3" style="padding: 0;">
                            <input type="text" class="form-control" required name="keys[]" placeholder="المهمة">
                        </div>
                        <div class="col-sm-8" style="padding-left: 0;">
                            <input type="text" class="form-control" required name="values[]" placeholder="تفاصيل المهمة">
                        </div>
                        <div class="col-md-1">
                            <span onclick="$('#sp-0').remove();" class="btn btn-sm btn-danger">
                                <span class="fa fa-trash"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div id="specifications">

                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-2">
                        <span onclick="addNewSpecification()" class="btn btn-sm btn-primary"><span
                                class="fa fa-plus"></span> إضافة مهمة جديدة </span>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">تعديل</button>
                        <a href="{{route('department.section.index')}}" class="btn btn-default">الغاء الامر</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection()

@section("js")
    <script>

        function addNewSpecification() {
            const rand = "{{\Illuminate\Support\Str::random(111)}}";
            $('#specifications').append('<div class="form-group" id="' + rand + '">\n' +
                '                    <div class="col-md-2">\n' +
                '                        <label for="fullname" class="control-label">  </label>\n' +
                '                    </div>\n' +
                '                    <div class="col-md-10" >\n' +
                '\n' +
                '                        <div class="col-md-3" style="padding: 0;" >\n' +
                '                            <input type="text" class="form-control" name="keys[]" placeholder="المهمة">\n' +
                '                        </div>\n' +
                '                        <div class="col-sm-8" style="padding-left: 0;">\n' +
                '                            <input type="text" class="form-control" name="values[]" placeholder="تفاصيل المهمة">\n' +
                '                        </div>\n' +
                '                        <div class="col-md-1">\n' +
                '                            <span onclick="$(\'#' + rand + '\').remove();" class="btn btn-sm btn-danger"><span\n' +
                '                                        class="fa fa-trash"></span></span>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>');
        }
    </script>
@endsection()

