@extends("Layouts._layout")

@section("title")
    عرض المشروع
@endsection()

@section("css")

@endsection()


@section("content")

@section("breadcramb")
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="{{route('home')}}">الرئيسية</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="{{route('admin.department.index')}}">الأقسام</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <span> تفاصيل مشروع </span>
            </li>
        </ul>
    </div>
@endsection()
<div class="row">
<div class="col-md-12">
    <div class="portlet light portlet-fit ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-share font-dark font-green"></i>
                <span class="caption-subject bold font-green uppercase"> اسم المشروع </span>
            </div>

        </div>
        <div class="portlet-body">
            <div class="timeline">
                <!-- TIMELINE ITEM -->
                <div class="timeline-item">
                    <div class="timeline-badge">
                        <img class="timeline-badge-userpic" src="{{asset('admin/img/user.png')}}"> </div>
                    <div class="timeline-body">
                        <div class="timeline-body-arrow"> </div>
                        <div class="timeline-body-head">
                            <div class="timeline-body-head-caption">
                                <a href="javascript:;" class="timeline-body-title font-blue-madison">أبو ضياء</a>
                                <span class="timeline-body-time font-grey-cascade">كتب على 7:45 PM</span>
                            </div>
                            <div class="timeline-body-head-actions">
                                <div class="btn-group" style="width: 100px;">
                                        <div class="note note-info task-department" style="border-color: #f5f6fa;">
                                            <span class="label label-primary">
                                                <a href=""><i class="glyphicon glyphicon-eye-open"></i></a>
                                                <a href=""><i class="glyphicon glyphicon-edit"></i></a>
                                                <form action="{{route('admin.department.destroy',1)}}" method="POST" class="form-horizontal" style="float:left; margin: 0px 0px -20px 10px;">
                                                            @csrf
                                                            @method('DELETE')
                                                            <div class="form-group">
                                                                <div class="col-sm-offset-2 col-sm-10">
                                                                    <button type="submit" class=""
                                                                            onclick="return confirm('هل أنت متأكد من حذف هذا العنصر !')"><i
                                                                            class="glyphicon glyphicon-trash"></i></button>
                                                                </div>
                                                            </div>
                                                        </form>
                                            </span>

                                </div>
                                </div>
                                {{--<div class="btn-group">
                                    <button class="btn btn-circle green btn-sm dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="javascript:;">Action </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Another action </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Something else here </a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="javascript:;">Separated link </a>
                                        </li>
                                    </ul>
                                </div>--}}
                            </div>
                        </div>
                        <div class="timeline-body-content">
                            <span class="font-grey-cascade"> هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، </span>
                        </div>
                    </div>
                </div>
                <!-- END TIMELINE ITEM -->
                <div class="timeline-item">
                    <div class="timeline-badge">
                        <img class="timeline-badge-userpic" src="{{asset('admin/img/user.png')}}"> </div>
                    <div class="timeline-body">
                        <div class="timeline-body-arrow"> </div>
                        <div class="timeline-body-head">
                            <div class="timeline-body-head-caption">
                                <a href="javascript:;" class="timeline-body-title font-blue-madison">أبو ضياء</a>
                                <span class="timeline-body-time font-grey-cascade">كتب على 7:45 PM</span>
                            </div>
                            <div class="timeline-body-head-actions">
                                <div class="btn-group" style="width: 100px;">
                                    <div class="note note-info task-department" style="border-color: #f5f6fa;">
                                            <span class="label label-primary">
                                                <a href=""><i class="glyphicon glyphicon-eye-open"></i></a>
                                                <a href=""><i class="glyphicon glyphicon-edit"></i></a>
                                                <form action="{{route('admin.department.destroy',1)}}" method="POST" class="form-horizontal" style="float:left; margin: 0px 0px -20px 10px;">
                                                            @csrf
                                                    @method('DELETE')
                                                            <div class="form-group">
                                                                <div class="col-sm-offset-2 col-sm-10">
                                                                    <button type="submit" class=""
                                                                            onclick="return confirm('هل أنت متأكد من حذف هذا العنصر !')"><i
                                                                            class="glyphicon glyphicon-trash"></i></button>
                                                                </div>
                                                            </div>
                                                        </form>
                                            </span>

                                    </div>
                                </div>
                                {{--<div class="btn-group">
                                    <button class="btn btn-circle green btn-sm dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="javascript:;">Action </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Another action </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Something else here </a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="javascript:;">Separated link </a>
                                        </li>
                                    </ul>
                                </div>--}}
                            </div>
                        </div>
                        <div class="timeline-body-content">
                            <span class="font-grey-cascade"> هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، </span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item">
                    <div class="timeline-badge">
                        <img class="timeline-badge-userpic" src="{{asset('admin/img/user.png')}}"> </div>
                    <div class="timeline-body">
                        <div class="timeline-body-arrow"> </div>
                        <div class="timeline-body-head">
                            <div class="timeline-body-head-caption">
                                <a href="javascript:;" class="timeline-body-title font-blue-madison">أبو ضياء</a>
                                <span class="timeline-body-time font-grey-cascade">كتب على 7:45 PM</span>
                            </div>
                            <div class="timeline-body-head-actions">
                                <div class="btn-group" style="width: 100px;">
                                    <div class="note note-info task-department" style="border-color: #f5f6fa;">
                                            <span class="label label-primary">
                                                <a href=""><i class="glyphicon glyphicon-eye-open"></i></a>
                                                <a href=""><i class="glyphicon glyphicon-edit"></i></a>
                                                <form action="{{route('admin.department.destroy',1)}}" method="POST" class="form-horizontal" style="float:left; margin: 0px 0px -20px 10px;">
                                                            @csrf
                                                    @method('DELETE')
                                                            <div class="form-group">
                                                                <div class="col-sm-offset-2 col-sm-10">
                                                                    <button type="submit" class=""
                                                                            onclick="return confirm('هل أنت متأكد من حذف هذا العنصر !')"><i
                                                                            class="glyphicon glyphicon-trash"></i></button>
                                                                </div>
                                                            </div>
                                                        </form>
                                            </span>

                                    </div>
                                </div>
                                {{--<div class="btn-group">
                                    <button class="btn btn-circle green btn-sm dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="javascript:;">Action </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Another action </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Something else here </a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="javascript:;">Separated link </a>
                                        </li>
                                    </ul>
                                </div>--}}
                            </div>
                        </div>
                        <div class="timeline-body-content">
                            <span class="font-grey-cascade"> هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، </span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item">
                    <div class="timeline-badge">
                        <img class="timeline-badge-userpic" src="{{asset('admin/img/user.png')}}"> </div>
                    <div class="timeline-body">
                        <div class="timeline-body-arrow"> </div>
                        <div class="timeline-body-head">
                            <div class="timeline-body-head-caption">
                                <a href="javascript:;" class="timeline-body-title font-blue-madison">أبو ضياء</a>
                                <span class="timeline-body-time font-grey-cascade">كتب على 7:45 PM</span>
                            </div>
                            <div class="timeline-body-head-actions">
                                <div class="btn-group" style="width: 100px;">
                                    <div class="note note-info task-department" style="border-color: #f5f6fa;">
                                            <span class="label label-primary">
                                                <a href=""><i class="glyphicon glyphicon-eye-open"></i></a>
                                                <a href=""><i class="glyphicon glyphicon-edit"></i></a>
                                                <form action="{{route('admin.department.destroy',1)}}" method="POST" class="form-horizontal" style="float:left; margin: 0px 0px -20px 10px;">
                                                            @csrf
                                                    @method('DELETE')
                                                            <div class="form-group">
                                                                <div class="col-sm-offset-2 col-sm-10">
                                                                    <button type="submit" class=""
                                                                            onclick="return confirm('هل أنت متأكد من حذف هذا العنصر !')"><i
                                                                            class="glyphicon glyphicon-trash"></i></button>
                                                                </div>
                                                            </div>
                                                        </form>
                                            </span>

                                    </div>
                                </div>
                                {{--<div class="btn-group">
                                    <button class="btn btn-circle green btn-sm dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="javascript:;">Action </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Another action </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Something else here </a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="javascript:;">Separated link </a>
                                        </li>
                                    </ul>
                                </div>--}}
                            </div>
                        </div>
                        <div class="timeline-body-content">
                            <span class="font-grey-cascade"> هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة،هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، </span>
                        </div>
                    </div>
                </div>






            </div>
        </div>
    </div>
</div>
</div>
@endsection()


@section("js")

@endsection()
