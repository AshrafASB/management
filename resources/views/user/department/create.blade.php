@extends("layouts.public")

@section("title")
    اضافة قسم جديدة
@endsection()

@section("css")

@endsection

@section("content")
    <div class="content">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>تنبيه!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div department="dialog">
            <div class="modal-dialog" style="width: 80% ;font-size: larger">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row">
                            <div class="col-lg-12 margin-tb">
                                <div class="pull-left">
                                    <h1 class="text-center" style="font-family: 'Al-Jazeera'">
                                        {{"إضافة قسم جديد"}}
                                    </h1>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-primary" href="{{ route('admin.department.index') }}"> رجوع</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        {!!Form::open(array('route' => 'admin.department.store','method'=>'POST','class'=>'form-horizontal','department'=>'form')) !!}
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="title">اسم القسم</label>
                            <div class="col-sm-9">
                                {!! Form::text('name', null, array('placeholder' => 'اسم القسم','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="title">وصف القسم</label>
                            <div class="col-sm-9">
                                {!! Form::text('description', null, array('placeholder' => 'وصف القسم','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="set_id" class="col-sm-3 control-label">إختر الدائرة </label>
                            <div class="col-sm-9">
                                {!!Form::select('set_id',$sets, null, ['placeholder' => '-- إختر الدائرة --','class'=>'form-control'])!!}
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success add">
                                <span class='glyphicon glyphicon-check'></span>حفظ
                            </button>

                            <a class="btn btn-primary" href="{{ route('admin.department.index') }}"> <span
                                        class='glyphicon glyphicon-remove'></span> رجوع</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
