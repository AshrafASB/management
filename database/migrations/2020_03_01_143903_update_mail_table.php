<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMailTable extends Migration
{
    public function up()
    {
        Schema::table('massages', function (Blueprint $table) {
            $table->boolean('isStarred');
        });
    }

    public function down()
    {

    }
}
