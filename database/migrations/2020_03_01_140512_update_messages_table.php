<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMessagesTable extends Migration
{

    public function up()
    {
        Schema::table('massages', function (Blueprint $table) {
            $table->boolean('isRead');
            $table->boolean('spam');
            $table->softDeletes();
        });
    }


    public function down()
    {
        Schema::table('massages', function (Blueprint $table) {
            //
        });
    }
}
