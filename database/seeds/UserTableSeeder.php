<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
$role1 =        \App\Role::create([
            'name' => 'admin',
    'description' => 'الادمن'

        ]);
$set1 = \App\Set::create([
    'name' => 'mony',
    'description' => 'دائرة المالية'
]);
$depart1 = \App\Department::create([
    'name' => 'Web',
    'description' => 'قسم الويب',
    'set_id' => $set1->id
]);
        \App\User::create([
            'name' => 'mosab',
            'email' => 'test@test.com',
            'password' => \Illuminate\Support\Facades\Hash::make('test@test.com'),
            'department_id' => $depart1->id,
            'set_id' => $set1->id,
            'role_id' => $role1->id

        ]);
    }
}
